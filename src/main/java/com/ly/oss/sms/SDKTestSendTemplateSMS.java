package com.ly.oss.sms;

import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class SDKTestSendTemplateSMS {
	
	//开发者主账号
//	public static final String ACCOUNT_SID = "8a48b5514c6a0c54014c6d842fa8016f";// dev
//	public static final String AUTH_TOKEN = "5bd9d26e901a46fbbf6d5d24519568c5";// dev
//	public static final String DEV_REST_URL = "sandboxapp.cloopen.com";//dev//测试地址
//	public static final String APP_ID = "8a48b5514c6a0c54014c6d894cf9017c";//appid //dev
//	public static final String TEMPLATE_ID = "1";//dev
	
	public static final String ACCOUNT_SID = "aaf98f894c6a0a1a014c6d81c5f80120";
	public static final String AUTH_TOKEN = "3188c70857b042a68ece26363af42b5b";
	public static final String DEV_REST_URL = "app.cloopen.com";//地址
	public static final String DEV_REST_PROT = "8883";
	public static final String APP_ID = "8a48b5514c7d3d77014c9342d0de0c0b";	//app id

	//短信模板id
	public static final String TEMPLATE_ID = "16385";
	
	//验证码有效时间 5 分钟
	public static final String EFFECTIVE_TIME="15";
	
	//验证码位数
	public static final int NUM = 6;
	
	
	public static String getVcode(){
		
		StringBuffer str = new StringBuffer();
		
		Random r =new Random();
		for (int i = 0; i < 6; i++) {
			int n =r.nextInt();
			
			n = (n<0?n*(-1):n)%10;
			
			str.append(n);
		}
		return str.toString();
	}
	
	public static void sendSms(String to,String vcode){
		HashMap<String, Object> result = null;

		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init(DEV_REST_URL, DEV_REST_PROT);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(ACCOUNT_SID, AUTH_TOKEN);// 初始化主帐号名称和主帐号令牌
		restAPI.setAppId(APP_ID);// 初始化应用ID
		result = restAPI.sendTemplateSMS(to,TEMPLATE_ID ,new String[]{vcode,EFFECTIVE_TIME});

		System.out.println("SDKTestGetSubAccounts result=" + result);
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap<String, Object> result = null;

		CCPRestSDK restAPI = new CCPRestSDK();
		restAPI.init(DEV_REST_URL, DEV_REST_PROT);// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(ACCOUNT_SID, AUTH_TOKEN);// 初始化主帐号名称和主帐号令牌
		restAPI.setAppId(APP_ID);// 初始化应用ID
		result = restAPI.sendTemplateSMS("13739199287","1" ,new String[]{getVcode(),"2"});

		System.out.println("SDKTestGetSubAccounts result=" + result);
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}
	}

}

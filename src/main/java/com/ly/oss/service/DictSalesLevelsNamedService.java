package com.ly.oss.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.DictSalesLevelsNamed;
import com.ly.oss.repository.DictSalesLevelsNamedDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class DictSalesLevelsNamedService {

	private static Logger logger = LoggerFactory.getLogger(DictSalesLevelsNamedService.class);

	@Autowired
	private DictSalesLevelsNamedDao dictSalesLevelsNamedDao;

	public DictSalesLevelsNamed getById(Long id) {
		return dictSalesLevelsNamedDao.getById(id);
	}

	public List<DictSalesLevelsNamed> getAll() {
		return dictSalesLevelsNamedDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<DictSalesLevelsNamed> searchPage(DictSalesLevelsNamed dictSalesLevelsNamed, int currentPage, int pageSize) {
		MyPage<DictSalesLevelsNamed> myPage = new MyPage<DictSalesLevelsNamed>();

		Long count = dictSalesLevelsNamedDao.searchCount(dictSalesLevelsNamed);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<DictSalesLevelsNamed> list = dictSalesLevelsNamedDao.searchPage(dictSalesLevelsNamed, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(DictSalesLevelsNamed dictSalesLevelsNamed) {
		dictSalesLevelsNamedDao.save(dictSalesLevelsNamed);
	}

	public void update(DictSalesLevelsNamed dictSalesLevelsNamed) {
		dictSalesLevelsNamedDao.update(dictSalesLevelsNamed);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		dictSalesLevelsNamedDao.delete(id);
	}
}

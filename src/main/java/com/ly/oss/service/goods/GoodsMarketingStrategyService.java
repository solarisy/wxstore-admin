package com.ly.oss.service.goods;

import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.GoodsMarketingStrategy;
import com.ly.oss.repository.GoodsMarketingStrategyDao;
import com.ly.oss.service.account.ShiroDbRealm.ShiroUser;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsMarketingStrategyService {

	private static Logger logger = LoggerFactory.getLogger(GoodsMarketingStrategyService.class);

	@Autowired
	private GoodsMarketingStrategyDao goodsMarketingStrategyDao;

	public GoodsMarketingStrategy getByGoodsId(Long goodsId) {
		return goodsMarketingStrategyDao.getByGoodsId(goodsId);
	}

	public List<GoodsMarketingStrategy> getAll() {
		return goodsMarketingStrategyDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<GoodsMarketingStrategy> searchPage(GoodsMarketingStrategy goodsMarketingStrategy, int currentPage, int pageSize) {
		MyPage<GoodsMarketingStrategy> myPage = new MyPage<GoodsMarketingStrategy>();

		Long count = goodsMarketingStrategyDao.searchCount(goodsMarketingStrategy);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<GoodsMarketingStrategy> list = goodsMarketingStrategyDao.searchPage(goodsMarketingStrategy, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(GoodsMarketingStrategy goodsMarketingStrategy) {
		goodsMarketingStrategyDao.save(goodsMarketingStrategy);
	}

	public void update(GoodsMarketingStrategy goodsMarketingStrategy) {
		goodsMarketingStrategy.setUpdateDate(new Date());
		goodsMarketingStrategy.setUpdateUserUid(getCurrentUserUid());
		if(goodsMarketingStrategy.getId()==null){
			goodsMarketingStrategy.setCreateDate(new Date());
			goodsMarketingStrategy.setCreateUserUid(getCurrentUserUid());
			goodsMarketingStrategyDao.save(goodsMarketingStrategy);
		}
		goodsMarketingStrategyDao.update(goodsMarketingStrategy);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsMarketingStrategyDao.delete(id);
	}
	
	public String getCurrentUserUid() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user.getUid();
	}
}

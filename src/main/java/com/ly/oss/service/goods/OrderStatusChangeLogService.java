package com.ly.oss.service.goods;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.OrderStatusChangeLog;
import com.ly.oss.repository.OrderStatusChangeLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class OrderStatusChangeLogService {

	private static Logger logger = LoggerFactory.getLogger(OrderStatusChangeLogService.class);

	@Autowired
	private OrderStatusChangeLogDao orderStatusChangeLogDao;

	public OrderStatusChangeLog getById(Long id) {
		return orderStatusChangeLogDao.getById(id);
	}

	public List<OrderStatusChangeLog> getAll() {
		return orderStatusChangeLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<OrderStatusChangeLog> searchPage(OrderStatusChangeLog orderStatusChangeLog, int currentPage, int pageSize) {
		MyPage<OrderStatusChangeLog> myPage = new MyPage<OrderStatusChangeLog>();

		Long count = orderStatusChangeLogDao.searchCount(orderStatusChangeLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<OrderStatusChangeLog> list = orderStatusChangeLogDao.searchPage(orderStatusChangeLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(OrderStatusChangeLog orderStatusChangeLog) {
		orderStatusChangeLogDao.save(orderStatusChangeLog);
	}

	public void update(OrderStatusChangeLog orderStatusChangeLog) {
		orderStatusChangeLogDao.update(orderStatusChangeLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		orderStatusChangeLogDao.delete(id);
	}
}

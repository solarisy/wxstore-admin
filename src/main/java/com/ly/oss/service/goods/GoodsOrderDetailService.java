package com.ly.oss.service.goods;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.GoodsOrderDetail;
import com.ly.oss.repository.GoodsOrderDetailDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsOrderDetailService {

	private static Logger logger = LoggerFactory.getLogger(GoodsOrderDetailService.class);

	@Autowired
	private GoodsOrderDetailDao goodsOrderDetailDao;

	public GoodsOrderDetail getById(Long id) {
		return goodsOrderDetailDao.getById(id);
	}

	public List<GoodsOrderDetail> getAll() {
		return goodsOrderDetailDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<GoodsOrderDetail> searchPage(GoodsOrderDetail goodsOrderDetail, int currentPage, int pageSize) {
		MyPage<GoodsOrderDetail> myPage = new MyPage<GoodsOrderDetail>();

		Long count = goodsOrderDetailDao.searchCount(goodsOrderDetail);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<GoodsOrderDetail> list = goodsOrderDetailDao.searchPage(goodsOrderDetail, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(GoodsOrderDetail goodsOrderDetail) {
		goodsOrderDetailDao.save(goodsOrderDetail);
	}

	public void update(GoodsOrderDetail goodsOrderDetail) {
		goodsOrderDetailDao.update(goodsOrderDetail);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsOrderDetailDao.delete(id);
	}

	public List<GoodsOrderDetail> getByGoodsOrderId(Long goodsOrderId) {
		// TODO Auto-generated method stub
		return goodsOrderDetailDao.getByGoodsOrderId(goodsOrderId);
	}
}

package com.ly.oss.service.goods;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.UserAddress;
import com.ly.oss.repository.UserAddressDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class UserAddressService {

	private static Logger logger = LoggerFactory.getLogger(UserAddressService.class);

	@Autowired
	private UserAddressDao userAddressDao;

	public UserAddress getById(Long id) {
		return userAddressDao.getById(id);
	}

	public List<UserAddress> getAll() {
		return userAddressDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<UserAddress> searchPage(UserAddress userAddress, int currentPage, int pageSize) {
		MyPage<UserAddress> myPage = new MyPage<UserAddress>();

		Long count = userAddressDao.searchCount(userAddress);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<UserAddress> list = userAddressDao.searchPage(userAddress, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(UserAddress userAddress) {
		userAddressDao.save(userAddress);
	}

	public void update(UserAddress userAddress) {
		userAddressDao.update(userAddress);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		userAddressDao.delete(id);
	}
}

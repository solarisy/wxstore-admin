package com.ly.oss.service.goods;

import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.Delete;
import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.Goods;
import com.ly.oss.repository.GoodsDao;
import com.ly.oss.service.account.ShiroDbRealm.ShiroUser;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsService {

	private static Logger logger = LoggerFactory.getLogger(GoodsService.class);

	@Autowired
	private GoodsDao goodsDao;

	public Goods getById(Long id) {
		return goodsDao.getById(id);
	}

	public List<Goods> getAll() {
		return goodsDao.getAll();
	}
	
	public List<Goods> getByGoodsName(String name) {
		return goodsDao.getByGoodsName(name);
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<Goods> searchPage(Goods goods, int currentPage, int pageSize) {
		MyPage<Goods> myPage = new MyPage<Goods>();

		Long count = goodsDao.searchCount(goods);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<Goods> list = goodsDao.searchPage(goods, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(Goods goods) {
		goodsDao.save(goods);
	}

	public void update(Goods goods) {
		
		goods.setUpdateDate(new Date());
		goods.setUpdateUserUid(getCurrentUserUid());
		goods.setDeleted(Delete.Enabled);
		
		if(goods.getId()==null){
			goods.setCreateUserUid(getCurrentUserUid());
			goods.setCreateDate(new Date());
			goodsDao.save(goods);
		}else{
			goodsDao.update(goods);
		}
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsDao.delete(id);
	}
	
	public String getCurrentUserUid() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user.getUid();
	}
}

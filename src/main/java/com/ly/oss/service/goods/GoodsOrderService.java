package com.ly.oss.service.goods;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.GoodsOrder;
import com.ly.oss.repository.GoodsOrderDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class GoodsOrderService {

	private static Logger logger = LoggerFactory.getLogger(GoodsOrderService.class);

	@Autowired
	private GoodsOrderDao goodsOrderDao;

	public GoodsOrder getById(Long id) {
		return goodsOrderDao.getById(id);
	}

	public List<GoodsOrder> getAll() {
		return goodsOrderDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<GoodsOrder> searchPage(GoodsOrder goodsOrder, int currentPage, int pageSize) {
		MyPage<GoodsOrder> myPage = new MyPage<GoodsOrder>();

		Long count = goodsOrderDao.searchCount(goodsOrder);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<GoodsOrder> list = goodsOrderDao.searchPage(goodsOrder, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(GoodsOrder goodsOrder) {
		goodsOrderDao.save(goodsOrder);
	}

	public void update(GoodsOrder goodsOrder) {
		goodsOrderDao.update(goodsOrder);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		goodsOrderDao.delete(id);
	}
}

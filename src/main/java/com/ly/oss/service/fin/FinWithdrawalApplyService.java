package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinWithdrawalApply;
import com.ly.oss.repository.FinWithdrawalApplyDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinWithdrawalApplyService {

	private static Logger logger = LoggerFactory.getLogger(FinWithdrawalApplyService.class);

	@Autowired
	private FinWithdrawalApplyDao finWithdrawalApplyDao;

	public FinWithdrawalApply getById(Long id) {
		return finWithdrawalApplyDao.getById(id);
	}

	public List<FinWithdrawalApply> getAll() {
		return finWithdrawalApplyDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinWithdrawalApply> searchPage(FinWithdrawalApply finWithdrawalApply, int currentPage, int pageSize) {
		MyPage<FinWithdrawalApply> myPage = new MyPage<FinWithdrawalApply>();

		Long count = finWithdrawalApplyDao.searchCount(finWithdrawalApply);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinWithdrawalApply> list = finWithdrawalApplyDao.searchPage(finWithdrawalApply, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinWithdrawalApply finWithdrawalApply) {
		finWithdrawalApplyDao.save(finWithdrawalApply);
	}

	public void update(FinWithdrawalApply finWithdrawalApply) {
		finWithdrawalApplyDao.update(finWithdrawalApply);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finWithdrawalApplyDao.delete(id);
	}

	public List<FinWithdrawalApply> getByWeixinPublicId(Long weixinPublicId) {
		return finWithdrawalApplyDao.getByWeixinPublicId(weixinPublicId);
	}
}

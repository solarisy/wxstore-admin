package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinBrokerageChangeLog;
import com.ly.oss.repository.FinBrokerageChangeLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinBrokerageChangeLogService {

	private static Logger logger = LoggerFactory.getLogger(FinBrokerageChangeLogService.class);

	@Autowired
	private FinBrokerageChangeLogDao finBrokerageChangeLogDao;

	public FinBrokerageChangeLog getById(Long id) {
		return finBrokerageChangeLogDao.getById(id);
	}

	public List<FinBrokerageChangeLog> getAll() {
		return finBrokerageChangeLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinBrokerageChangeLog> searchPage(FinBrokerageChangeLog finBrokerageChangeLog, int currentPage, int pageSize) {
		MyPage<FinBrokerageChangeLog> myPage = new MyPage<FinBrokerageChangeLog>();

		Long count = finBrokerageChangeLogDao.searchCount(finBrokerageChangeLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinBrokerageChangeLog> list = finBrokerageChangeLogDao.searchPage(finBrokerageChangeLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinBrokerageChangeLog finBrokerageChangeLog) {
		finBrokerageChangeLogDao.save(finBrokerageChangeLog);
	}

	public void update(FinBrokerageChangeLog finBrokerageChangeLog) {
		finBrokerageChangeLogDao.update(finBrokerageChangeLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finBrokerageChangeLogDao.delete(id);
	}
}

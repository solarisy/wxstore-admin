package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinBrokerageRecord;
import com.ly.oss.repository.FinBrokerageRecordDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinBrokerageRecordService {

	private static Logger logger = LoggerFactory.getLogger(FinBrokerageRecordService.class);

	@Autowired
	private FinBrokerageRecordDao finBrokerageRecordDao;

	public FinBrokerageRecord getById(Long id) {
		return finBrokerageRecordDao.getById(id);
	}

	public List<FinBrokerageRecord> getAll() {
		return finBrokerageRecordDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinBrokerageRecord> searchPage(FinBrokerageRecord finBrokerageRecord, int currentPage, int pageSize) {
		MyPage<FinBrokerageRecord> myPage = new MyPage<FinBrokerageRecord>();

		Long count = finBrokerageRecordDao.searchCount(finBrokerageRecord);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinBrokerageRecord> list = finBrokerageRecordDao.searchPage(finBrokerageRecord, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinBrokerageRecord finBrokerageRecord) {
		finBrokerageRecordDao.save(finBrokerageRecord);
	}

	public void update(FinBrokerageRecord finBrokerageRecord) {
		finBrokerageRecordDao.update(finBrokerageRecord);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finBrokerageRecordDao.delete(id);
	}
}

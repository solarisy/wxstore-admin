package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinPaymentMethod;
import com.ly.oss.repository.FinPaymentMethodDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinPaymentMethodService {

	private static Logger logger = LoggerFactory.getLogger(FinPaymentMethodService.class);

	@Autowired
	private FinPaymentMethodDao finPaymentMethodDao;

	public FinPaymentMethod getById(Long id) {
		return finPaymentMethodDao.getById(id);
	}

	public List<FinPaymentMethod> getAll() {
		return finPaymentMethodDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinPaymentMethod> searchPage(FinPaymentMethod finPaymentMethod, int currentPage, int pageSize) {
		MyPage<FinPaymentMethod> myPage = new MyPage<FinPaymentMethod>();

		Long count = finPaymentMethodDao.searchCount(finPaymentMethod);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinPaymentMethod> list = finPaymentMethodDao.searchPage(finPaymentMethod, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinPaymentMethod finPaymentMethod) {
		finPaymentMethodDao.save(finPaymentMethod);
	}

	public void update(FinPaymentMethod finPaymentMethod) {
		finPaymentMethodDao.update(finPaymentMethod);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finPaymentMethodDao.delete(id);
	}
}

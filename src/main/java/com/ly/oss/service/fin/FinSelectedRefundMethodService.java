package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinSelectedRefundMethod;
import com.ly.oss.repository.FinSelectedRefundMethodDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinSelectedRefundMethodService {

	private static Logger logger = LoggerFactory.getLogger(FinSelectedRefundMethodService.class);

	@Autowired
	private FinSelectedRefundMethodDao finSelectedRefundMethodDao;

	public FinSelectedRefundMethod getById(Long id) {
		return finSelectedRefundMethodDao.getById(id);
	}

	public List<FinSelectedRefundMethod> getAll() {
		return finSelectedRefundMethodDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinSelectedRefundMethod> searchPage(FinSelectedRefundMethod finSelectedRefundMethod, int currentPage, int pageSize) {
		MyPage<FinSelectedRefundMethod> myPage = new MyPage<FinSelectedRefundMethod>();

		Long count = finSelectedRefundMethodDao.searchCount(finSelectedRefundMethod);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinSelectedRefundMethod> list = finSelectedRefundMethodDao.searchPage(finSelectedRefundMethod, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinSelectedRefundMethod finSelectedRefundMethod) {
		finSelectedRefundMethodDao.save(finSelectedRefundMethod);
	}

	public void update(FinSelectedRefundMethod finSelectedRefundMethod) {
		finSelectedRefundMethodDao.update(finSelectedRefundMethod);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finSelectedRefundMethodDao.delete(id);
	}
}

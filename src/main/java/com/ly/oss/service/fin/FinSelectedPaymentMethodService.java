package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinSelectedPaymentMethod;
import com.ly.oss.repository.FinSelectedPaymentMethodDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinSelectedPaymentMethodService {

	private static Logger logger = LoggerFactory.getLogger(FinSelectedPaymentMethodService.class);

	@Autowired
	private FinSelectedPaymentMethodDao finSelectedPaymentMethodDao;

	public FinSelectedPaymentMethod getById(Long id) {
		return finSelectedPaymentMethodDao.getById(id);
	}

	public List<FinSelectedPaymentMethod> getAll() {
		return finSelectedPaymentMethodDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinSelectedPaymentMethod> searchPage(FinSelectedPaymentMethod finSelectedPaymentMethod, int currentPage, int pageSize) {
		MyPage<FinSelectedPaymentMethod> myPage = new MyPage<FinSelectedPaymentMethod>();

		Long count = finSelectedPaymentMethodDao.searchCount(finSelectedPaymentMethod);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinSelectedPaymentMethod> list = finSelectedPaymentMethodDao.searchPage(finSelectedPaymentMethod, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinSelectedPaymentMethod finSelectedPaymentMethod) {
		finSelectedPaymentMethodDao.save(finSelectedPaymentMethod);
	}

	public void update(FinSelectedPaymentMethod finSelectedPaymentMethod) {
		finSelectedPaymentMethodDao.update(finSelectedPaymentMethod);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finSelectedPaymentMethodDao.delete(id);
	}
}

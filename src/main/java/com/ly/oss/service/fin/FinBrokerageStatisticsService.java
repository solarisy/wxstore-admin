package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinBrokerageStatistics;
import com.ly.oss.repository.FinBrokerageStatisticsDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinBrokerageStatisticsService {

	private static Logger logger = LoggerFactory.getLogger(FinBrokerageStatisticsService.class);

	@Autowired
	private FinBrokerageStatisticsDao finBrokerageStatisticsDao;

	public FinBrokerageStatistics getById(Long id) {
		return finBrokerageStatisticsDao.getById(id);
	}

	public List<FinBrokerageStatistics> getAll() {
		return finBrokerageStatisticsDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinBrokerageStatistics> searchPage(FinBrokerageStatistics finBrokerageStatistics, int currentPage, int pageSize) {
		MyPage<FinBrokerageStatistics> myPage = new MyPage<FinBrokerageStatistics>();

		Long count = finBrokerageStatisticsDao.searchCount(finBrokerageStatistics);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinBrokerageStatistics> list = finBrokerageStatisticsDao.searchPage(finBrokerageStatistics, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinBrokerageStatistics finBrokerageStatistics) {
		finBrokerageStatisticsDao.save(finBrokerageStatistics);
	}

	public void update(FinBrokerageStatistics finBrokerageStatistics) {
		finBrokerageStatisticsDao.update(finBrokerageStatistics);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finBrokerageStatisticsDao.delete(id);
	}
}

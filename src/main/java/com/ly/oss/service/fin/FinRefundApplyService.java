package com.ly.oss.service.fin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.FinRefundApply;
import com.ly.oss.repository.FinRefundApplyDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class FinRefundApplyService {

	private static Logger logger = LoggerFactory.getLogger(FinRefundApplyService.class);

	@Autowired
	private FinRefundApplyDao finRefundApplyDao;

	public FinRefundApply getById(Long id) {
		return finRefundApplyDao.getById(id);
	}

	public List<FinRefundApply> getAll() {
		return finRefundApplyDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<FinRefundApply> searchPage(FinRefundApply finRefundApply, int currentPage, int pageSize) {
		MyPage<FinRefundApply> myPage = new MyPage<FinRefundApply>();

		Long count = finRefundApplyDao.searchCount(finRefundApply);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<FinRefundApply> list = finRefundApplyDao.searchPage(finRefundApply, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(FinRefundApply finRefundApply) {
		finRefundApplyDao.save(finRefundApply);
	}

	public void update(FinRefundApply finRefundApply) {
		finRefundApplyDao.update(finRefundApply);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		finRefundApplyDao.delete(id);
	}
}

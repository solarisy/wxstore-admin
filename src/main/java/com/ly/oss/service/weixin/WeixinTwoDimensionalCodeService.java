package com.ly.oss.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.WeixinTwoDimensionalCode;
import com.ly.oss.repository.WeixinTwoDimensionalCodeDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinTwoDimensionalCodeService {

	private static Logger logger = LoggerFactory.getLogger(WeixinTwoDimensionalCodeService.class);

	@Autowired
	private WeixinTwoDimensionalCodeDao weixinTwoDimensionalCodeDao;

	public WeixinTwoDimensionalCode getById(Long id) {
		return weixinTwoDimensionalCodeDao.getById(id);
	}

	public List<WeixinTwoDimensionalCode> getAll() {
		return weixinTwoDimensionalCodeDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinTwoDimensionalCode> searchPage(WeixinTwoDimensionalCode weixinTwoDimensionalCode, int currentPage, int pageSize) {
		MyPage<WeixinTwoDimensionalCode> myPage = new MyPage<WeixinTwoDimensionalCode>();

		Long count = weixinTwoDimensionalCodeDao.searchCount(weixinTwoDimensionalCode);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinTwoDimensionalCode> list = weixinTwoDimensionalCodeDao.searchPage(weixinTwoDimensionalCode, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinTwoDimensionalCode weixinTwoDimensionalCode) {
		weixinTwoDimensionalCodeDao.save(weixinTwoDimensionalCode);
	}

	public void update(WeixinTwoDimensionalCode weixinTwoDimensionalCode) {
		weixinTwoDimensionalCodeDao.update(weixinTwoDimensionalCode);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinTwoDimensionalCodeDao.delete(id);
	}
}

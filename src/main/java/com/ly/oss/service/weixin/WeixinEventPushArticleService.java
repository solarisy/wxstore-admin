package com.ly.oss.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.WeixinEventPushArticle;
import com.ly.oss.repository.WeixinEventPushArticleDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinEventPushArticleService {

	private static Logger logger = LoggerFactory.getLogger(WeixinEventPushArticleService.class);

	@Autowired
	private WeixinEventPushArticleDao weixinEventPushArticleDao;

	public WeixinEventPushArticle getById(Long id) {
		return weixinEventPushArticleDao.getById(id);
	}

	public List<WeixinEventPushArticle> getAll() {
		return weixinEventPushArticleDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinEventPushArticle> searchPage(WeixinEventPushArticle weixinEventPushArticle, int currentPage, int pageSize) {
		MyPage<WeixinEventPushArticle> myPage = new MyPage<WeixinEventPushArticle>();

		Long count = weixinEventPushArticleDao.searchCount(weixinEventPushArticle);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinEventPushArticle> list = weixinEventPushArticleDao.searchPage(weixinEventPushArticle, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinEventPushArticle weixinEventPushArticle) {
		weixinEventPushArticleDao.save(weixinEventPushArticle);
	}

	public void update(WeixinEventPushArticle weixinEventPushArticle) {
		weixinEventPushArticleDao.update(weixinEventPushArticle);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinEventPushArticleDao.delete(id);
	}
}

package com.ly.oss.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.WeixinUserInfo;
import com.ly.oss.repository.WeixinUserInfoDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinUserInfoService {

	private static Logger logger = LoggerFactory.getLogger(WeixinUserInfoService.class);

	@Autowired
	private WeixinUserInfoDao weixinUserInfoDao;

	public WeixinUserInfo getById(Long id) {
		return weixinUserInfoDao.getById(id);
	}

	public List<WeixinUserInfo> getAll() {
		return weixinUserInfoDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinUserInfo> searchPage(WeixinUserInfo weixinUserInfo, int currentPage, int pageSize) {
		MyPage<WeixinUserInfo> myPage = new MyPage<WeixinUserInfo>();

		Long count = weixinUserInfoDao.searchCount(weixinUserInfo);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinUserInfo> list = weixinUserInfoDao.searchPage(weixinUserInfo, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinUserInfo weixinUserInfo) {
		weixinUserInfoDao.save(weixinUserInfo);
	}

	public void update(WeixinUserInfo weixinUserInfo) {
		weixinUserInfoDao.update(weixinUserInfo);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinUserInfoDao.delete(id);
	}
}

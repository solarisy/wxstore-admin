package com.ly.oss.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.WeixinScanLog;
import com.ly.oss.repository.WeixinScanLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinScanLogService {

	private static Logger logger = LoggerFactory.getLogger(WeixinScanLogService.class);

	@Autowired
	private WeixinScanLogDao weixinScanLogDao;

	public WeixinScanLog getById(Long id) {
		return weixinScanLogDao.getById(id);
	}

	public List<WeixinScanLog> getAll() {
		return weixinScanLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinScanLog> searchPage(WeixinScanLog weixinScanLog, int currentPage, int pageSize) {
		MyPage<WeixinScanLog> myPage = new MyPage<WeixinScanLog>();

		Long count = weixinScanLogDao.searchCount(weixinScanLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinScanLog> list = weixinScanLogDao.searchPage(weixinScanLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinScanLog weixinScanLog) {
		weixinScanLogDao.save(weixinScanLog);
	}

	public void update(WeixinScanLog weixinScanLog) {
		weixinScanLogDao.update(weixinScanLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinScanLogDao.delete(id);
	}
}

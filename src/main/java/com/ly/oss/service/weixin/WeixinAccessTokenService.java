package com.ly.oss.service.weixin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.WeixinAccessToken;
import com.ly.oss.repository.WeixinAccessTokenDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinAccessTokenService {

	private static Logger logger = LoggerFactory.getLogger(WeixinAccessTokenService.class);

	@Autowired
	private WeixinAccessTokenDao weixinAccessTokenDao;

	public WeixinAccessToken getById(Long id) {
		return weixinAccessTokenDao.getById(id);
	}

	public List<WeixinAccessToken> getAll() {
		return weixinAccessTokenDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinAccessToken> searchPage(WeixinAccessToken weixinAccessToken, int currentPage, int pageSize) {
		MyPage<WeixinAccessToken> myPage = new MyPage<WeixinAccessToken>();

		Long count = weixinAccessTokenDao.searchCount(weixinAccessToken);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinAccessToken> list = weixinAccessTokenDao.searchPage(weixinAccessToken, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinAccessToken weixinAccessToken) {
		weixinAccessTokenDao.save(weixinAccessToken);
	}

	public void update(WeixinAccessToken weixinAccessToken) {
		weixinAccessTokenDao.update(weixinAccessToken);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinAccessTokenDao.delete(id);
	}
}

package com.ly.oss.service.account;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.SalerDownHierarchy;
import com.ly.oss.repository.SalerDownHierarchyDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class SalerDownHierarchyService {

	private static Logger logger = LoggerFactory.getLogger(SalerDownHierarchyService.class);

	@Autowired
	private SalerDownHierarchyDao salerDownHierarchyDao;

	public SalerDownHierarchy getById(Long id) {
		return salerDownHierarchyDao.getById(id);
	}

	public List<SalerDownHierarchy> getAll() {
		return salerDownHierarchyDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<SalerDownHierarchy> searchPage(SalerDownHierarchy salerDownHierarchy, int currentPage, int pageSize) {
		MyPage<SalerDownHierarchy> myPage = new MyPage<SalerDownHierarchy>();

		Long count = salerDownHierarchyDao.searchCount(salerDownHierarchy);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<SalerDownHierarchy> list = salerDownHierarchyDao.searchPage(salerDownHierarchy, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(SalerDownHierarchy salerDownHierarchy) {
		salerDownHierarchyDao.save(salerDownHierarchy);
	}

	public void update(SalerDownHierarchy salerDownHierarchy) {
		salerDownHierarchyDao.update(salerDownHierarchy);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		salerDownHierarchyDao.delete(id);
	}
}

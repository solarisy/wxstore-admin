package com.ly.oss.service.account;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.OssAccount;
import com.ly.oss.repository.OssAccountDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class OssAccountService {

	private static Logger logger = LoggerFactory.getLogger(OssAccountService.class);

	@Autowired
	private OssAccountDao ossAccountDao;

	public OssAccount getById(Long id) {
		return ossAccountDao.getById(id);
	}

	public List<OssAccount> getAll() {
		return ossAccountDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<OssAccount> searchPage(OssAccount ossAccount, int currentPage, int pageSize) {
		MyPage<OssAccount> myPage = new MyPage<OssAccount>();

		Long count = ossAccountDao.searchCount(ossAccount);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<OssAccount> list = ossAccountDao.searchPage(ossAccount, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(OssAccount ossAccount) {
		ossAccountDao.save(ossAccount);
	}

	public void update(OssAccount ossAccount) {
		ossAccountDao.update(ossAccount);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		ossAccountDao.delete(id);
	}
}

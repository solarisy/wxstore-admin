/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.oss.service.account;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.security.utils.Digests;
import org.springside.modules.utils.Clock;
import org.springside.modules.utils.Encodes;

import com.ly.oss.comm.Delete;
import com.ly.oss.entity.OssAccount;
import com.ly.oss.repository.OssAccountDao;
import com.ly.oss.service.ServiceException;
import com.ly.oss.service.account.ShiroDbRealm.ShiroUser;

/**
 * 用户管理类.
 * 
 * @author calvin
 */
// Spring Service Bean的标识.
@Component
@Transactional
public class AccountService {

	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	private static final int SALT_SIZE = 8;

	private static Logger logger = LoggerFactory.getLogger(AccountService.class);

	private OssAccountDao ossAccountDao;
	private Clock clock = Clock.DEFAULT;

	public List<OssAccount> getAllUser() {
		return (List<OssAccount>) ossAccountDao.getAll();
	}

	public OssAccount getUser(Long id) {
		return ossAccountDao.getById(id);
	}

	public OssAccount findUserByLoginName(String loginName) {
		return ossAccountDao.findByLoginName(loginName);
	}

	public void registerUser(OssAccount user) {
		entryptPassword(user);
		user.setRoles("user");
		user.setCreateDate(clock.getCurrentDate());
		user.setUpdateDate(clock.getCurrentDate());
		user.setDeleted(Delete.Enabled);
		user.setUid(UUID.randomUUID().toString());
		
		ossAccountDao.save(user);
	}

	public void updateUser(OssAccount user) {
		if (StringUtils.isNotBlank(user.getPlainPassword())) {
			entryptPassword(user);
		}
		if(user.getId()==null){
			ossAccountDao.save(user);
		}else{
			ossAccountDao.update(user);
		}
	}

	public void deleteUser(Long id) {
		if (isSupervisor(id)) {
			logger.warn("操作员{}尝试删除超级管理员用户", getCurrentUserName());
			throw new ServiceException("不能删除超级管理员用户");
		}
		ossAccountDao.delete(id);
	}

	/**
	 * 判断是否超级管理员.
	 */
	private boolean isSupervisor(Long id) {
		return id == 1;
	}

	/**
	 * 取出Shiro中的当前用户LoginName.
	 */
	private String getCurrentUserName() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user.loginName;
	}

	/**
	 * 设定安全的密码，生成随机的salt并经过1024次 sha-1 hash
	 */
	private void entryptPassword(OssAccount user) {
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		user.setSalt(Encodes.encodeHex(salt));

		byte[] hashPassword = Digests.sha1(user.getPlainPassword().getBytes(), salt, HASH_INTERATIONS);
		user.setPassword(Encodes.encodeHex(hashPassword));
	}

	@Autowired
	public void setOssAccountDao(OssAccountDao ossAccountDao) {
		this.ossAccountDao = ossAccountDao;
	}

	public void setClock(Clock clock) {
		this.clock = clock;
	}

	
}

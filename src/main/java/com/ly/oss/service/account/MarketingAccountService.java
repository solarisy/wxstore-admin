package com.ly.oss.service.account;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.MarketingAccount;
import com.ly.oss.repository.MarketingAccountDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class MarketingAccountService {

	private static Logger logger = LoggerFactory.getLogger(MarketingAccountService.class);

	@Autowired
	private MarketingAccountDao marketingAccountDao;

	public MarketingAccount getById(Long id) {
		return marketingAccountDao.getById(id);
	}

	public List<MarketingAccount> getAll() {
		return marketingAccountDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<MarketingAccount> searchPage(MarketingAccount marketingAccount, int currentPage, int pageSize) {
		MyPage<MarketingAccount> myPage = new MyPage<MarketingAccount>();

		Long count = marketingAccountDao.searchCount(marketingAccount);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<MarketingAccount> list = marketingAccountDao.searchPage(marketingAccount, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(MarketingAccount marketingAccount) {
		marketingAccountDao.save(marketingAccount);
	}

	public void update(MarketingAccount marketingAccount) {
		marketingAccountDao.update(marketingAccount);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		marketingAccountDao.delete(id);
	}

	public List<MarketingAccount> getByWeixinPublicId(Long weixinPublicId) {
		return marketingAccountDao.getByWeixinPublicId(weixinPublicId);
	}
}

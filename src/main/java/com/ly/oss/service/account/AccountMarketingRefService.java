package com.ly.oss.service.account;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.AccountMarketingRef;
import com.ly.oss.repository.AccountMarketingRefDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class AccountMarketingRefService {

	private static Logger logger = LoggerFactory.getLogger(AccountMarketingRefService.class);

	@Autowired
	private AccountMarketingRefDao accountMarketingRefDao;

	public AccountMarketingRef getById(Long id) {
		return accountMarketingRefDao.getById(id);
	}

	public List<AccountMarketingRef> getAll() {
		return accountMarketingRefDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<AccountMarketingRef> searchPage(AccountMarketingRef accountMarketingRef, int currentPage, int pageSize) {
		MyPage<AccountMarketingRef> myPage = new MyPage<AccountMarketingRef>();

		Long count = accountMarketingRefDao.searchCount(accountMarketingRef);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<AccountMarketingRef> list = accountMarketingRefDao.searchPage(accountMarketingRef, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(AccountMarketingRef accountMarketingRef) {
		accountMarketingRefDao.save(accountMarketingRef);
	}

	public void update(AccountMarketingRef accountMarketingRef) {
		accountMarketingRefDao.update(accountMarketingRef);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		accountMarketingRefDao.delete(id);
	}
}

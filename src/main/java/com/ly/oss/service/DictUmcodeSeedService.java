package com.ly.oss.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.ly.oss.comm.MyPage;
import com.ly.oss.entity.DictUmcodeSeed;
import com.ly.oss.repository.DictUmcodeSeedDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class DictUmcodeSeedService {

	private static Logger logger = LoggerFactory.getLogger(DictUmcodeSeedService.class);

	@Autowired
	private DictUmcodeSeedDao dictUmcodeSeedDao;

	public DictUmcodeSeed getById(Long id) {
		return dictUmcodeSeedDao.getById(id);
	}

	public List<DictUmcodeSeed> getAll() {
		return dictUmcodeSeedDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<DictUmcodeSeed> searchPage(DictUmcodeSeed dictUmcodeSeed, int currentPage, int pageSize) {
		MyPage<DictUmcodeSeed> myPage = new MyPage<DictUmcodeSeed>();

		Long count = dictUmcodeSeedDao.searchCount(dictUmcodeSeed);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<DictUmcodeSeed> list = dictUmcodeSeedDao.searchPage(dictUmcodeSeed, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(DictUmcodeSeed dictUmcodeSeed) {
		dictUmcodeSeedDao.save(dictUmcodeSeed);
	}

	public void update(DictUmcodeSeed dictUmcodeSeed) {
		dictUmcodeSeedDao.update(dictUmcodeSeed);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		dictUmcodeSeedDao.delete(id);
	}
}

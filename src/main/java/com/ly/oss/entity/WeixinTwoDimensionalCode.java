package com.ly.oss.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinTwoDimensionalCode {

	public WeixinTwoDimensionalCode() {
	}
	
	private Long id; //
	private Date createDate; //
	private Date updateDate; //
	private String imageName; //
	private String openid; //
	private String sceneStr; //
	private String imageUrl; //
	private Long weixinPublicId; //微信公众号id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *
     **/
	public String getImageName(){
		return imageName;
	}
	
	/**
	 *
	 **/
	public void setImageName(String imageName){
		this.imageName=imageName;
	}
    /**
     *
     **/
	public String getOpenid(){
		return openid;
	}
	
	/**
	 *
	 **/
	public void setOpenid(String openid){
		this.openid=openid;
	}
    /**
     *
     **/
	public String getSceneStr(){
		return sceneStr;
	}
	
	/**
	 *
	 **/
	public void setSceneStr(String sceneStr){
		this.sceneStr=sceneStr;
	}
    /**
     *
     **/
	public String getImageUrl(){
		return imageUrl;
	}
	
	/**
	 *
	 **/
	public void setImageUrl(String imageUrl){
		this.imageUrl=imageUrl;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
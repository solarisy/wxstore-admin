package com.ly.oss.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class GoodsOrderDetail {

	public GoodsOrderDetail() {
	}

	private Long id; //
	private Long orderId; //
	private Long goodsId; //
	private Integer goodsNumber = 0; //

	private Double realPrice = 0.0; // 单价
	private String goodsName; //
	private String goodsDesc; //

	public Double getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(Double realPrice) {
		this.realPrice = realPrice;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsDesc() {
		return goodsDesc;
	}

	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	/**
	 * 小计
	 * 
	 * @return
	 */
	public Double getTotal() {
		if (goodsNumber == null) {
			return 0.0;
		}

		return goodsNumber * realPrice;
	}

	/**
     *
     **/
	public Long getId() {
		return id;
	}

	/**
	 *
	 **/
	public void setId(Long id) {
		this.id = id;
	}

	/**
     *
     **/
	public Long getOrderId() {
		return orderId;
	}

	/**
	 *
	 **/
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	/**
     *
     **/
	public Long getGoodsId() {
		return goodsId;
	}

	/**
	 *
	 **/
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	/**
     *
     **/
	public Integer getGoodsNumber() {
		return goodsNumber;
	}

	/**
	 *
	 **/
	public void setGoodsNumber(Integer goodsNumber) {
		this.goodsNumber = goodsNumber;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
package com.ly.oss.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class SalerDownHierarchy {

	public SalerDownHierarchy() {
	}
	
	private Long id; //
	private String sid; //
	private String downLevelSid; //用户营销号编UserMarkingCode(umcode)
	private Long level; //
	private Date createDate; //创建时间
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public String getSid(){
		return sid;
	}
	
	/**
	 *
	 **/
	public void setSid(String sid){
		this.sid=sid;
	}
    /**
     *用户营销号编UserMarkingCode(umcode)
     **/
	public String getDownLevelSid(){
		return downLevelSid;
	}
	
	/**
	 *用户营销号编UserMarkingCode(umcode)
	 **/
	public void setDownLevelSid(String downLevelSid){
		this.downLevelSid=downLevelSid;
	}
    /**
     *
     **/
	public Long getLevel(){
		return level;
	}
	
	/**
	 *
	 **/
	public void setLevel(Long level){
		this.level=level;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
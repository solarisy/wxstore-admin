package com.ly.oss.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class WeixinScanLog {

	public WeixinScanLog() {
	}
	
	private Long id; //
	private Date createDate; //
	private Date updateDate; //
	private String openid; //
	private String sceneStr; //场景值
	private String parentSceneStr; //父场景值
	private String caseName; //扫码情景：（已关注扫码，取消关注扫码，新用户扫码）
	private String caseCode; //扫码情景code
	private String currentParentSceneStr; //
	private Long weixinPublicId; //微信公众号id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *
     **/
	public String getOpenid(){
		return openid;
	}
	
	/**
	 *
	 **/
	public void setOpenid(String openid){
		this.openid=openid;
	}
    /**
     *场景值
     **/
	public String getSceneStr(){
		return sceneStr;
	}
	
	/**
	 *场景值
	 **/
	public void setSceneStr(String sceneStr){
		this.sceneStr=sceneStr;
	}
    /**
     *父场景值
     **/
	public String getParentSceneStr(){
		return parentSceneStr;
	}
	
	/**
	 *父场景值
	 **/
	public void setParentSceneStr(String parentSceneStr){
		this.parentSceneStr=parentSceneStr;
	}
    /**
     *扫码情景：（已关注扫码，取消关注扫码，新用户扫码）
     **/
	public String getCaseName(){
		return caseName;
	}
	
	/**
	 *扫码情景：（已关注扫码，取消关注扫码，新用户扫码）
	 **/
	public void setCaseName(String caseName){
		this.caseName=caseName;
	}
    /**
     *扫码情景code
     **/
	public String getCaseCode(){
		return caseCode;
	}
	
	/**
	 *扫码情景code
	 **/
	public void setCaseCode(String caseCode){
		this.caseCode=caseCode;
	}
    /**
     *
     **/
	public String getCurrentParentSceneStr(){
		return currentParentSceneStr;
	}
	
	/**
	 *
	 **/
	public void setCurrentParentSceneStr(String currentParentSceneStr){
		this.currentParentSceneStr=currentParentSceneStr;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
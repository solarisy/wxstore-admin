package com.ly.oss.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class DictSalesLevelsNamed {

	public DictSalesLevelsNamed() {
	}
	
	private Long id; //
	private Date createDate; //创建时间
	private Date updateDate; //最后更新时间
	private String createUserUid; //创建人uid
	private String updateUserUid; //最后更新人uid
	private Integer deleted; //是否删除（0：删，1：用）
	private Long weixinPublicId; //
	private String level; //
	private String nickname; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *最后更新时间
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *最后更新时间
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *创建人uid
     **/
	public String getCreateUserUid(){
		return createUserUid;
	}
	
	/**
	 *创建人uid
	 **/
	public void setCreateUserUid(String createUserUid){
		this.createUserUid=createUserUid;
	}
    /**
     *最后更新人uid
     **/
	public String getUpdateUserUid(){
		return updateUserUid;
	}
	
	/**
	 *最后更新人uid
	 **/
	public void setUpdateUserUid(String updateUserUid){
		this.updateUserUid=updateUserUid;
	}
    /**
     *是否删除（0：删，1：用）
     **/
	public Integer getDeleted(){
		return deleted;
	}
	
	/**
	 *是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted){
		this.deleted=deleted;
	}
    /**
     *
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
    /**
     *
     **/
	public String getLevel(){
		return level;
	}
	
	/**
	 *
	 **/
	public void setLevel(String level){
		this.level=level;
	}
    /**
     *
     **/
	public String getNickname(){
		return nickname;
	}
	
	/**
	 *
	 **/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
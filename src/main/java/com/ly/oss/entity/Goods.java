package com.ly.oss.entity;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.ly.oss.comm.GoodsStatus;

public class Goods {

	public Goods() {
	}
	
	private Long id; //
	private Date createDate; //创建时间
	private Date updateDate; //最后更新时间
	private String createUserUid; //创建人uid
	private String updateUserUid; //最后更新人uid
	private Integer deleted; //是否删除（0：删，1：用）
	private Long weixinPublicId; //微信公众号id
	private String goodsName; //
	private String goodsDesc; //
	private Double price; //
	private Double realPrice; //
	private String goodsDetailsUrl; //
	private Long goodsStatus; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *最后更新时间
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *最后更新时间
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *创建人uid
     **/
	public String getCreateUserUid(){
		return createUserUid;
	}
	
	/**
	 *创建人uid
	 **/
	public void setCreateUserUid(String createUserUid){
		this.createUserUid=createUserUid;
	}
    /**
     *最后更新人uid
     **/
	public String getUpdateUserUid(){
		return updateUserUid;
	}
	
	/**
	 *最后更新人uid
	 **/
	public void setUpdateUserUid(String updateUserUid){
		this.updateUserUid=updateUserUid;
	}
    /**
     *是否删除（0：删，1：用）
     **/
	public Integer getDeleted(){
		return deleted;
	}
	
	/**
	 *是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted){
		this.deleted=deleted;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
    /**
     *
     **/
	public String getGoodsName(){
		return goodsName;
	}
	
	/**
	 *
	 **/
	public void setGoodsName(String goodsName){
		this.goodsName=goodsName;
	}
    /**
     *
     **/
	public String getGoodsDesc(){
		return goodsDesc;
	}
	
	/**
	 *
	 **/
	public void setGoodsDesc(String goodsDesc){
		this.goodsDesc=goodsDesc;
	}
    /**
     *
     **/
	public Double getPrice(){
		return price;
	}
	
	/**
	 *
	 **/
	public void setPrice(Double price){
		this.price=price;
	}
    /**
     *
     **/
	public Double getRealPrice(){
		return realPrice;
	}
	
	/**
	 *
	 **/
	public void setRealPrice(Double realPrice){
		this.realPrice=realPrice;
	}
    /**
     *
     **/
	public String getGoodsDetailsUrl(){
		return goodsDetailsUrl;
	}
	
	/**
	 *
	 **/
	public void setGoodsDetailsUrl(String goodsDetailsUrl){
		this.goodsDetailsUrl=goodsDetailsUrl;
	}
    /**
     *
     **/
	public Long getGoodsStatus(){
		return goodsStatus;
	}
	
	/**
	 *
	 **/
	public void setGoodsStatus(Long goodsStatus){
		this.goodsStatus=goodsStatus;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private String goodsStatusName; //
	private String publicName; //
	private Double levelOneBrokerage; //一级佣金
	private Double levelTwoBrokerage; //二级佣金
	private Double levelThreeBrokerage; //三级佣金
	
	public Double getLevelOneBrokerage() {
		return levelOneBrokerage;
	}

	public void setLevelOneBrokerage(Double levelOneBrokerage) {
		this.levelOneBrokerage = levelOneBrokerage;
	}

	public Double getLevelTwoBrokerage() {
		return levelTwoBrokerage;
	}

	public void setLevelTwoBrokerage(Double levelTwoBrokerage) {
		this.levelTwoBrokerage = levelTwoBrokerage;
	}

	public Double getLevelThreeBrokerage() {
		return levelThreeBrokerage;
	}

	public void setLevelThreeBrokerage(Double levelThreeBrokerage) {
		this.levelThreeBrokerage = levelThreeBrokerage;
	}

	public String getGoodsStatusName() {
		goodsStatusName = "";
		if (goodsStatus != null) {
			if (goodsStatus.longValue() == GoodsStatus.Enabled) {
				goodsStatusName = GoodsStatus.Enabled_name;
			} else if (goodsStatus.longValue() == GoodsStatus.Disabled) {
				goodsStatusName = GoodsStatus.Disabled_name;
			}
		}
		return goodsStatusName;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}
}
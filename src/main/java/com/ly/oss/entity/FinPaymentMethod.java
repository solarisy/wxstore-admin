package com.ly.oss.entity;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class FinPaymentMethod {

	public FinPaymentMethod() {
	}
	
	private Long id; //
	private Date createDate; //
	private String methodName; //
	private String methodDesc; //
	private String createUid; //
	private Date updateDate; //
	private String updateUid; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *
     **/
	public String getMethodName(){
		return methodName;
	}
	
	/**
	 *
	 **/
	public void setMethodName(String methodName){
		this.methodName=methodName;
	}
    /**
     *
     **/
	public String getMethodDesc(){
		return methodDesc;
	}
	
	/**
	 *
	 **/
	public void setMethodDesc(String methodDesc){
		this.methodDesc=methodDesc;
	}
    /**
     *
     **/
	public String getCreateUid(){
		return createUid;
	}
	
	/**
	 *
	 **/
	public void setCreateUid(String createUid){
		this.createUid=createUid;
	}
    /**
     *
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *
     **/
	public String getUpdateUid(){
		return updateUid;
	}
	
	/**
	 *
	 **/
	public void setUpdateUid(String updateUid){
		this.updateUid=updateUid;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
package com.ly.oss.entity;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.ly.oss.comm.OrderStatus;

public class GoodsOrder {

	public GoodsOrder() {
	}
	
	private Long id; //
	private Date createDate; //创建时间
	private Date updateDate; //最后更新时间
	private String updateSid; //最后更新人sid
	private String createSid; //创建人的场景值sid
	private Integer deleted; //是否删除（0：删，1：用）
	private Long weixinPublicId; //微信公众号id
	private Integer orderStatus; //订单状态(1.代付款，2.待发货，3.待收货，4.待评价)
	private Double money; //
	private Double realMoney; //
	private Integer canceled; //是否取消订单
	private String orderCode; //
	private String openid; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *最后更新时间
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *最后更新时间
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *最后更新人sid
     **/
	public String getUpdateSid(){
		return updateSid;
	}
	
	/**
	 *最后更新人sid
	 **/
	public void setUpdateSid(String updateSid){
		this.updateSid=updateSid;
	}
    /**
     *创建人的场景值sid
     **/
	public String getCreateSid(){
		return createSid;
	}
	
	/**
	 *创建人的场景值sid
	 **/
	public void setCreateSid(String createSid){
		this.createSid=createSid;
	}
    /**
     *是否删除（0：删，1：用）
     **/
	public Integer getDeleted(){
		return deleted;
	}
	
	/**
	 *是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted){
		this.deleted=deleted;
	}
    /**
     *微信公众号id
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *微信公众号id
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
    /**
     *订单状态(1.代付款，2.待发货，3.待收货，4.待评价)
     **/
	public Integer getOrderStatus(){
		return orderStatus;
	}
	
	/**
	 *订单状态(1.代付款，2.待发货，3.待收货，4.待评价)
	 **/
	public void setOrderStatus(Integer orderStatus){
		this.orderStatus=orderStatus;
	}
    /**
     *
     **/
	public Double getMoney(){
		return money;
	}
	
	/**
	 *
	 **/
	public void setMoney(Double money){
		this.money=money;
	}
    /**
     *
     **/
	public Double getRealMoney(){
		return realMoney;
	}
	
	/**
	 *
	 **/
	public void setRealMoney(Double realMoney){
		this.realMoney=realMoney;
	}
    /**
     *是否取消订单
     **/
	public Integer getCanceled(){
		return canceled;
	}
	
	/**
	 *是否取消订单
	 **/
	public void setCanceled(Integer canceled){
		this.canceled=canceled;
	}
    /**
     *
     **/
	public String getOrderCode(){
		return orderCode;
	}
	
	/**
	 *
	 **/
	public void setOrderCode(String orderCode){
		this.orderCode=orderCode;
	}
    /**
     *
     **/
	public String getOpenid(){
		return openid;
	}
	
	/**
	 *
	 **/
	public void setOpenid(String openid){
		this.openid=openid;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private String nickname; // 微信昵称
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	
	public String getOrderStatusName(){
		return OrderStatus.getOrderStatusName(orderStatus.toString());
	}
}
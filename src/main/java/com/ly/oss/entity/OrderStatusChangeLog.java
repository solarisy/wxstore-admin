package com.ly.oss.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class OrderStatusChangeLog {

	public OrderStatusChangeLog() {
	}
	
	private Long id; //
	private Long orderId; //
	private Long orderStatus; //
	private Date createDate; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Long getOrderId(){
		return orderId;
	}
	
	/**
	 *
	 **/
	public void setOrderId(Long orderId){
		this.orderId=orderId;
	}
    /**
     *
     **/
	public Long getOrderStatus(){
		return orderStatus;
	}
	
	/**
	 *
	 **/
	public void setOrderStatus(Long orderStatus){
		this.orderStatus=orderStatus;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
package com.ly.oss.entity;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class MarketingAccount {

	public MarketingAccount() {
	}
	
	private Long id; //
	private Long pid; //
	private String openid; //
	private String umcode; //用户营销号编UserMarkingCode(umcode)
	private String sid; //
	private String parentSid; //
	private Long weixinPublicId; //
	private Date createDate; //创建时间
	private Long boss; //是否拥有微信公众号（boss）1,是，0.不是
	private String name; //姓名
	private String phone; //手机
	private Long depth; //树的深度
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Long getPid(){
		return pid;
	}
	
	/**
	 *
	 **/
	public void setPid(Long pid){
		this.pid=pid;
	}
    /**
     *
     **/
	public String getOpenid(){
		return openid;
	}
	
	/**
	 *
	 **/
	public void setOpenid(String openid){
		this.openid=openid;
	}
    /**
     *用户营销号编UserMarkingCode(umcode)
     **/
	public String getUmcode(){
		return umcode;
	}
	
	/**
	 *用户营销号编UserMarkingCode(umcode)
	 **/
	public void setUmcode(String umcode){
		this.umcode=umcode;
	}
    /**
     *
     **/
	public String getSid(){
		return sid;
	}
	
	/**
	 *
	 **/
	public void setSid(String sid){
		this.sid=sid;
	}
    /**
     *
     **/
	public String getParentSid(){
		return parentSid;
	}
	
	/**
	 *
	 **/
	public void setParentSid(String parentSid){
		this.parentSid=parentSid;
	}
    /**
     *
     **/
	public Long getWeixinPublicId(){
		return weixinPublicId;
	}
	
	/**
	 *
	 **/
	public void setWeixinPublicId(Long weixinPublicId){
		this.weixinPublicId=weixinPublicId;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *是否拥有微信公众号（boss）1,是，0.不是
     **/
	public Long getBoss(){
		return boss;
	}
	
	/**
	 *是否拥有微信公众号（boss）1,是，0.不是
	 **/
	public void setBoss(Long boss){
		this.boss=boss;
	}
    /**
     *姓名
     **/
	public String getName(){
		return name;
	}
	
	/**
	 *姓名
	 **/
	public void setName(String name){
		this.name=name;
	}
    /**
     *手机
     **/
	public String getPhone(){
		return phone;
	}
	
	/**
	 *手机
	 **/
	public void setPhone(String phone){
		this.phone=phone;
	}
    /**
     *树的深度
     **/
	public Long getDepth(){
		return depth;
	}
	
	/**
	 *树的深度
	 **/
	public void setDepth(Long depth){
		this.depth=depth;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private String downCount; //下线人数
	private String sumRealMoney; //个人消费
	private String brokerage; //总佣金（有效佣金的历史累计）

	public String getDownCount() {
		return downCount;
	}

	public void setDownCount(String downCount) {
		this.downCount = downCount;
	}

	public String getSumRealMoney() {
		return sumRealMoney;
	}

	public void setSumRealMoney(String sumRealMoney) {
		this.sumRealMoney = sumRealMoney;
	}

	public String getBrokerage() {
		return brokerage;
	}

	public void setBrokerage(String brokerage) {
		this.brokerage = brokerage;
	}
	
	
}
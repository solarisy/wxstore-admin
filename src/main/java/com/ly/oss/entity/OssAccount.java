package com.ly.oss.entity;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.ImmutableList;

public class OssAccount {

	public OssAccount() {
	}
	
	private Long id; //
	private Date createDate; //创建时间
	private Date updateDate; //最后更新时间
	private Integer deleted; //是否删除（0：删，1：用）
	private String loginName; //
	private String password; //
	private String salt; //
	private String roles; //(user,platform_user)
	private String uid; //
	private String name; //
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *最后更新时间
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *最后更新时间
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *是否删除（0：删，1：用）
     **/
	public Integer getDeleted(){
		return deleted;
	}
	
	/**
	 *是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted){
		this.deleted=deleted;
	}
    /**
     *
     **/
	public String getLoginName(){
		return loginName;
	}
	
	/**
	 *
	 **/
	public void setLoginName(String loginName){
		this.loginName=loginName;
	}
    /**
     *
     **/
	public String getPassword(){
		return password;
	}
	
	/**
	 *
	 **/
	public void setPassword(String password){
		this.password=password;
	}
    /**
     *
     **/
	public String getSalt(){
		return salt;
	}
	
	/**
	 *
	 **/
	public void setSalt(String salt){
		this.salt=salt;
	}
    /**
     *(user,platform_user)
     **/
	public String getRoles(){
		return roles;
	}
	
	/**
	 *(user,platform_user)
	 **/
	public void setRoles(String roles){
		this.roles=roles;
	}
    /**
     *
     **/
	public String getUid(){
		return uid;
	}
	
	/**
	 *
	 **/
	public void setUid(String uid){
		this.uid=uid;
	}
    /**
     *
     **/
	public String getName(){
		return name;
	}
	
	/**
	 *
	 **/
	public void setName(String name){
		this.name=name;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	
	//-------------------------------以下是追加的代码-------------------------------------------------
	private String plainPassword;
	
	@Transient
	@JsonIgnore
	public List<String> getRoleList() {
		// 角色列表在数据库中实际以逗号分隔字符串存储，因此返回不能修改的List.
		return ImmutableList.copyOf(StringUtils.split(roles, ","));
	}

	@Transient
	public String getPlainPassword() {
		return plainPassword;
	}

	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}

}
package com.ly.oss.comm;


/**
 * 商品状态（1：上架；2：下架）
 * @author Administrator
 *
 */
public class GoodsStatus {
	
	/**
	 * 1：上架
	 */
	public static final Long Enabled=1L;
	
	/**
	 * 1：上架
	 */
	public static final String Enabled_name="上架";
	
	/**
	 * 2：下架
	 */
	public static final Long Disabled=2L;
	
	/**
	 * 1：下架
	 */
	public static final String Disabled_name="下架";

}

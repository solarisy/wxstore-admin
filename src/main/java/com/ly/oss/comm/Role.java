package com.ly.oss.comm;

import java.util.HashMap;
import java.util.Map;

/**
 * 定义系统的角色
 * 
 * @author Wang Peng 2014-9-1
 */
public class Role {
	private static Map<String, String> roleMap;
	static {
		roleMap = new HashMap<String, String>();
		roleMap.put(Role.ADMIN, Role.ADMIN_NAME);
		roleMap.put(Role.USER, Role.USER_NAME);
	}

	public static String getRoleName(String roleCode) {
		return roleMap.get(roleCode);
	}

	/**
	 * 系统管理员，该角色不对用户开发，仅仅针对系统维护人员开放
	 */
	public static final String ADMIN = "admin";
	public static final String ADMIN_NAME = "系统管理员";

	/**
	 * 系统管理员，该角色不对用户开发，仅仅针对系统维护人员开放
	 */
	public static final String USER = "user";
	public static final String USER_NAME = "用户";

}

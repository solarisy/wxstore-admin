package com.ly.oss.comm;

import java.util.HashMap;
import java.util.Map;

/**
 * 订单状态(1.待付款，2.待发货，3.待收货，4.待评价)
 * @author Administrator
 *
 */
public class OrderStatus {
	
	private static Map<String, String> orderStatusMap;
	static {
		orderStatusMap = new HashMap<String, String>();
		orderStatusMap.put("1", "待付款");
		orderStatusMap.put("2", "待发货");
		orderStatusMap.put("3", "待收货");
		orderStatusMap.put("4", "待评价");
	}

	public static String getOrderStatusName(String code) {
		return orderStatusMap.get(code);
	}

	/**
	 * 未付款
	 */
	public static final Integer UNPAY =1;
	
	/**
	 * 待发货
	 */
	public static final Integer WAITING_DELIVER =2;
	
	/**
	 * 待收货
	 */
	public static final Integer WAITING_RECEIVING =3;
	
	/**
	 * 待评价
	 */
	public static final Integer WAITING_FEEDBACK =4;
	
}

package com.ly.oss.comm;

import java.util.List;
import java.util.Map;

public class MySearchResult {

	public boolean success = true;
	public String msg = "";
	public List<Map<String, Object>> resultData;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<Map<String, Object>> getResultData() {
		return resultData;
	}

	public void setResultData(List<Map<String, Object>> resultData) {
		this.resultData = resultData;
	}

}

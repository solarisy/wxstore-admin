package com.ly.oss.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ly.oss.entity.WeixinUserInfo;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface WeixinUserInfoDao {
	
	WeixinUserInfo getById(Long id);
	
	List<WeixinUserInfo> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinUserInfo> searchPage(@Param("weixinUserInfo")WeixinUserInfo weixinUserInfo,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(WeixinUserInfo weixinUserInfo);
	
	void save(WeixinUserInfo weixinUserInfo);
	
	void update(WeixinUserInfo weixinUserInfo);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}

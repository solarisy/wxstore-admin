package com.ly.oss.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ly.oss.entity.WeixinScanLog;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface WeixinScanLogDao {
	
	WeixinScanLog getById(Long id);
	
	List<WeixinScanLog> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinScanLog> searchPage(@Param("weixinScanLog")WeixinScanLog weixinScanLog,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(WeixinScanLog weixinScanLog);
	
	void save(WeixinScanLog weixinScanLog);
	
	void update(WeixinScanLog weixinScanLog);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}

package com.ly.oss.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ly.oss.entity.AccountMarketingRef;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface AccountMarketingRefDao {
	
	AccountMarketingRef getById(Long id);
	
	List<AccountMarketingRef> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<AccountMarketingRef> searchPage(@Param("accountMarketingRef")AccountMarketingRef accountMarketingRef,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(AccountMarketingRef accountMarketingRef);
	
	void save(AccountMarketingRef accountMarketingRef);
	
	void update(AccountMarketingRef accountMarketingRef);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}

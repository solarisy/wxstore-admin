package com.ly.oss.repository;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ly.oss.entity.WeixinPublic;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface WeixinPublicDao {
	
	WeixinPublic getById(Long id);
	
	List<WeixinPublic> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinPublic> searchPage(@Param("weixinPublic")WeixinPublic weixinPublic,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(WeixinPublic weixinPublic);
	
	void save(WeixinPublic weixinPublic);
	
	void update(WeixinPublic weixinPublic);
	
	/**
	 * 软删除
	 */
	void delete(Long id);
	

}

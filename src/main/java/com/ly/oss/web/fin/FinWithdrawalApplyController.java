/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.oss.web.fin;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ly.oss.entity.FinWithdrawalApply;
import com.ly.oss.entity.WeixinPublic;
import com.ly.oss.service.account.ShiroDbRealm.ShiroUser;
import com.ly.oss.service.fin.FinWithdrawalApplyService;
import com.ly.oss.service.weixin.WeixinPublicService;

/**
 * 提现申请Controller.
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/fin")
public class FinWithdrawalApplyController {

	@Autowired
	private WeixinPublicService weixinPublicService;

	@Autowired
	private FinWithdrawalApplyService finWithdrawalApplyService;

	/**
	 * 提现申请列表
	 * @param weixinPublicId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="withdrawalApply/{weixinPublicId}", method = RequestMethod.GET)
	public String list(@PathVariable("weixinPublicId") Long weixinPublicId, Model model) {
		List<WeixinPublic> weixinPublicList = weixinPublicService.getAll();

		
		List<FinWithdrawalApply> finWithdrawalApplyList = finWithdrawalApplyService.getByWeixinPublicId(weixinPublicId);

		model.addAttribute("finWithdrawalApply", finWithdrawalApplyList);
		model.addAttribute("weixinPublicList", weixinPublicList);
		model.addAttribute("weixinPublicId", weixinPublicId);
		return "fin/withdrawalApplyList";
	}

	public ShiroUser getCurrentUser() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}

}

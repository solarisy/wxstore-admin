/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.oss.web.weixin;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ly.oss.entity.WeixinPublic;
import com.ly.oss.service.weixin.WeixinPublicService;

/**
 * 管理员管理用户的Controller.
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/weixin")
public class WeixinPublicController {

	@Autowired
	private WeixinPublicService weixinPublicService;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		List<WeixinPublic> weixinPublicList = weixinPublicService.getAll();
		model.addAttribute("weixinPublicList", weixinPublicList);

		return "weixinPublic/weixinPublicList";
	}
	
	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createForm(Model model) {
		return "weixinPublic/weixinPublicForm";
	}

	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("weixinPublic", weixinPublicService.getById(id));
		return "weixinPublic/weixinPublicForm";
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@Valid @ModelAttribute("weixinPublic") WeixinPublic weixinPublic, RedirectAttributes redirectAttributes) {
		weixinPublicService.update(weixinPublic);
		redirectAttributes.addFlashAttribute("message", "更新微信公众号【" + weixinPublic.getPublicName() + "】成功");
		return "redirect:/weixin";
	}

	@RequestMapping(value = "delete/{id}")
	public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		WeixinPublic weixinPublic = weixinPublicService.getById(id);
		weixinPublicService.delete(id);
		redirectAttributes.addFlashAttribute("message", "删除微信公众号【" + weixinPublic.getPublicName() + "】成功");
		return "redirect:/weixin";
	}

	
}

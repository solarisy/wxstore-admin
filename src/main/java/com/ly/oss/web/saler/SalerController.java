/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.oss.web.saler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ly.oss.entity.MarketingAccount;
import com.ly.oss.entity.WeixinPublic;
import com.ly.oss.service.account.MarketingAccountService;
import com.ly.oss.service.account.ShiroDbRealm.ShiroUser;
import com.ly.oss.service.weixin.WeixinPublicService;

/**
 * 管理员管理用户的Controller.
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/saler")
public class SalerController {

	@Autowired
	private MarketingAccountService marketingAccountService;

	@Autowired
	private WeixinPublicService weixinPublicService;

	@RequestMapping(method = RequestMethod.GET)
	public String list(HttpServletRequest request, Model model) {
		List<WeixinPublic> weixinPublicList = weixinPublicService.getAll();

		String weixinPublicIdStr = request.getParameter("weixinPublicId");
		List<MarketingAccount> marketingAccountList = null;
		if (StringUtils.isBlank(weixinPublicIdStr)) {
			marketingAccountList = marketingAccountService.getAll();
		} else {
			Long weixinPublicId = Long.valueOf(weixinPublicIdStr);
			marketingAccountList = marketingAccountService.getByWeixinPublicId(weixinPublicId);
		}

		model.addAttribute("marketingAccountList", marketingAccountList);
		model.addAttribute("weixinPublicList", weixinPublicList);
		model.addAttribute("weixinPublicIdStr", weixinPublicIdStr);
		return "saler/salerList";
	}

	public ShiroUser getCurrentUser() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}

}

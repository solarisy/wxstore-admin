/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.oss.web.goods;

import java.util.List;

import javax.validation.Valid;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ly.oss.entity.Goods;
import com.ly.oss.entity.GoodsMarketingStrategy;
import com.ly.oss.service.account.ShiroDbRealm.ShiroUser;
import com.ly.oss.service.goods.GoodsMarketingStrategyService;
import com.ly.oss.service.goods.GoodsService;
import com.ly.oss.service.weixin.WeixinPublicService;

/**
 * 管理员管理用户的Controller.
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/goods")
public class GoodsController {

	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private WeixinPublicService weixinPublicService;
	
	@Autowired
	private GoodsMarketingStrategyService goodsMarketingStrategyService;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		List<Goods> list = goodsService.getAll();
		model.addAttribute("list", list);
		return "goods/goodsList";
	}
	
	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createForm(Model model) {
		model.addAttribute("weixinPublicList", weixinPublicService.getAll());
		return "goods/goodsForm";
	}
	
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("weixinPublicList", weixinPublicService.getAll());
		model.addAttribute("goods", goodsService.getById(id));
		return "goods/goodsForm";
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@Valid @ModelAttribute("goods") Goods goods, RedirectAttributes redirectAttributes) {
		goodsService.update(goods);
		redirectAttributes.addFlashAttribute("message", "更新商品【" + goods.getGoodsName() + "】成功");
		return "redirect:/goods";
	}
	
	
	@RequestMapping(value = "delete/{id}")
	public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		Goods goods = goodsService.getById(id);
		goodsService.delete(id);
		redirectAttributes.addFlashAttribute("message", "删除商品【" + goods.getGoodsName() + "】成功");
		return "redirect:/goods";
	}
	
	@RequestMapping(value = "marketingStrategy/{goodsId}", method = RequestMethod.GET)
	public String marketingStrategyForm(@PathVariable("goodsId") Long goodsId, Model model) {
		model.addAttribute("goodsMarketingStrategy", goodsMarketingStrategyService.getByGoodsId(goodsId));
		model.addAttribute("goods", goodsService.getById(goodsId));
		return "goods/goodsMarketingStrategyForm";
	}
	
	@RequestMapping(value = "updateMarketingStrategy", method = RequestMethod.POST)
	public String updateMarketingStrategy(@Valid @ModelAttribute("updateMarketingStrategy") GoodsMarketingStrategy marketingStrategy, RedirectAttributes redirectAttributes) {
		goodsMarketingStrategyService.update(marketingStrategy);
		
		redirectAttributes.addFlashAttribute("message", "更新商品佣金成功");
		return "redirect:/goods";
	}
	
	
	public ShiroUser getCurrentUser() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}
	
}

/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.oss.web.goods;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ly.oss.entity.GoodsOrder;
import com.ly.oss.entity.GoodsOrderDetail;
import com.ly.oss.service.goods.GoodsOrderDetailService;
import com.ly.oss.service.goods.GoodsOrderService;

/**
 * 管理员管理用户的Controller.
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/order")
public class OrderController {

	@Autowired
	private GoodsOrderService goodsOrderService;
	@Autowired
	private GoodsOrderDetailService goodsOrderDetailService;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model) {
		List<GoodsOrder> list = goodsOrderService.getAll();
		model.addAttribute("list", list);
		return "order/orderList";
	}
	
	@RequestMapping(value = "detail/{id}")
	public String detail(@PathVariable("id") Long id, Model model) {
		Long allGoodsNumber = 0L;//共N件（商品数量）
		Double allRealPay = 0.0;//实付
		List<GoodsOrderDetail> detailList = goodsOrderDetailService.getByGoodsOrderId(id);
		for(GoodsOrderDetail goods : detailList){
			allGoodsNumber += goods.getGoodsNumber();
			allRealPay += goods.getTotal();
		}
		
		
		model.addAttribute("goodsOrder", goodsOrderService.getById(id));
		model.addAttribute("detailList", detailList);
		model.addAttribute("allGoodsNumber", allGoodsNumber);
		model.addAttribute("allRealPay", allRealPay);
		return "order/orderDetail";
	}
	
	
}

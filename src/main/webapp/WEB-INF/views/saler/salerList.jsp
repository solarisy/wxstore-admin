<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>销售员管理</title>

</head>

<script type="text/javascript" src="${ctx }/static/js/jquery.js"></script>
<script type="text/javascript">

</script>
   
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<%-- <c:if test="${not empty message}">
					<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close"></button>${message}</div>
				</c:if> --%>
				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">销售员管理</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>销售员管理</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span8">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-group"></i>销售员管理</div>
								<div class="tools">
									<select id="weixinPublicId" name="weixinPublicId" data-placeholder="选择所属公众号" class="chosen-with-diselect span10"  >
										<option value="0">---请选择---</option>
										<c:forEach items="${weixinPublicList}" var="weixinPublic" >
											<c:choose>
												<c:when test="${weixinPublic.id == weixinPublicIdStr}">
													<option selected="selected" value="${weixinPublic.id}">${weixinPublic.publicName}</option>
												</c:when>
												<c:otherwise>
													<option value="${weixinPublic.id}">${weixinPublic.publicName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							
							<div class="portlet-body">
								<table class="table table-striped table-bordered  table-hover" id="sample_2">
									<thead>
										<tr>
											<th>序号</th>
											<th>用户编号</th>
											<th>姓名</th>
											<th>手机</th>
											<th>树的深度</th>
											<th>下线人数</th>
											<th>总佣金</th>
											<th>个人消费</th>
											<th>创建时间</th>
											<!-- <th>操作</th> -->
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${marketingAccountList }" var="single" varStatus="status">
											<tr>
												<td>${status.count}</td>
												<td>${single.umcode }</td>
												<td>${single.name }</td>
												<td>${single.phone }</td>
												<td>${single.depth }</td>
												<td><c:if test="${empty single.downCount}">0</c:if>${single.downCount }</td>
												<td><c:if test="${empty single.sumRealMoney}">0</c:if>${single.sumRealMoney }</td>
												<td><c:if test="${empty single.brokerage}">0</c:if>${single.brokerage }</td>
												<td><fmt:formatDate value="${single.createDate }" pattern="yyyy-MM-dd HH:mm"/></td>
												<!-- <td></td> -->
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>
				
					<!-- zTree -->
					<div class="span4">
						<div class="portlet box grey">
							<div class="portlet-title">
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
								<div class="actions">
									<a href="javascript:;" id="tree_1_collapse" class="btn green"><i class=" icon-resize-full"></i> 展开</a>
									<a href="javascript:;" id="tree_1_expand" class="btn yellow"><i class="icon-resize-small"></i> 折叠</a>
								</div>
							</div>
							<div class="portlet-body fuelux">
								<ul id="treeDemo" class="ztree"></ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	
	<script type="text/javascript">
	$(document).ready(function() {
		$("#weixinPublicId").bind('change',function(){
			var wpid=$("#weixinPublicId").val();
			window.location="${ctx}/saler?weixinPublicId="+wpid;
		});
	});

	$(document).ready(function() {
		initTable2();
	});
	
	var initTable2 = function() {
	    var oTable = $('#sample_2').dataTable( {           
	        "aoColumnDefs": [
	            { "aTargets": [ 0 ] }
	        ],
	        "aaSorting": [[0, 'asc']],
	         "aLengthMenu": [
	            [5, 15, 20, -1],
	            [5, 15, 20, "All"] // change per page values here
	        ],
	        // set the initial value
	        "iDisplayLength": 10,
	    });
	
	    jQuery('#sample_2_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
	    jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
	    jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown
	
	    $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
	        /* Get the DataTables object again - this is not a recreation, just a get of the object */
	        var iCol = parseInt($(this).attr("data-column"));
	        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
	        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
	    });
	}
	
	function delConfirm(del_url){
		bootbox.dialog({
			  message: "删除后不可恢复，是否删除数据？",
			  title: "删除确认",
			  buttons: {
			    success: {
			      label: "取消",
			      className: "btn"
			    },
			    danger: {
			      label: "删除",
			      className: "btn black",
			      callback: function() {
			    	  location.href = del_url;
			      }
			    }
			  }
		});
	}
	
</script>
<SCRIPT type="text/javascript">
		<!--
		var setting = {
			data: {
				simpleData: {
					enable: true
				}
			}
		};
		
		var zNodes =[
		<c:forEach items="${marketingAccountList }" var="single" varStatus="status">
			{ id:${single.id}, pId:${single.pid}, name:"${single.name}"},
		</c:forEach>
		];
		

		/*
		var zNodes =[
			{ id:1, pId:0, name:"父节点1 - 展开", open:true},
			{ id:11, pId:1, name:"父节点11 - 折叠"},
			{ id:111, pId:11, name:"叶子节点111"},
			{ id:112, pId:11, name:"叶子节点112"},
			{ id:113, pId:11, name:"叶子节点113"},
			{ id:114, pId:11, name:"叶子节点114"},
			{ id:12, pId:1, name:"父节点12 - 折叠"},
			{ id:121, pId:12, name:"叶子节点121"},
			{ id:122, pId:12, name:"叶子节点122"},
			{ id:123, pId:12, name:"叶子节点123"},
			{ id:124, pId:12, name:"叶子节点124"},
			{ id:13, pId:1, name:"父节点13 - 没有子节点"},
			{ id:2, pId:0, name:"父节点2 - 折叠"},
			{ id:21, pId:2, name:"父节点21 - 展开", open:true},
			{ id:211, pId:21, name:"叶子节点211"},
			{ id:212, pId:21, name:"叶子节点212"},
			{ id:213, pId:21, name:"叶子节点213"},
			{ id:214, pId:21, name:"叶子节点214"},
			{ id:22, pId:2, name:"父节点22 - 折叠"},
			{ id:221, pId:22, name:"叶子节点221"},
			{ id:222, pId:22, name:"叶子节点222"},
			{ id:223, pId:22, name:"叶子节点223"},
			{ id:224, pId:22, name:"叶子节点224"},
			{ id:23, pId:2, name:"父节点23 - 折叠"},
			{ id:231, pId:23, name:"叶子节点231"},
			{ id:232, pId:23, name:"叶子节点232"},
			{ id:233, pId:23, name:"叶子节点233"},
			{ id:234, pId:23, name:"叶子节点234"},
			{ id:3, pId:0, name:"父节点3 - 没有子节点", isParent:true}
		];
		*/

		$(document).ready(function(){
			var treeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
			
			$("#tree_1_collapse").bind('click',function(){
				treeObj.expandAll(true);
			});
			$("#tree_1_expand").bind('click',function(){
				treeObj.expandAll(false);
			});
		});
		//-->
	</SCRIPT>
</body>
	
</html>


<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>提现申请审核</title>

</head>

<script type="text/javascript" src="${ctx }/static/js/jquery.js"></script>
<script type="text/javascript">

</script>
   
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<%-- <c:if test="${not empty message}">
					<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close"></button>${message}</div>
				</c:if> --%>
				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">提现申请审核</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>提现申请审核</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-euro"></i>提现申请审核</div>
								<div class="tools">
									<select id="weixinPublicId" name="weixinPublicId" data-placeholder="选择所属公众号" class="chosen-with-diselect span10" >
										<option value="0">---请选择---</option>
										<c:forEach items="${weixinPublicList}" var="weixinPublic" >
											<c:choose>
												<c:when test="${weixinPublic.id == weixinPublicId}">
													<option selected="selected" value="${weixinPublic.id}">${weixinPublic.publicName}</option>
												</c:when>
												<c:otherwise>
													<option value="${weixinPublic.id}">${weixinPublic.publicName}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							
							<div class="portlet-body">
								<table class="table table-striped table-bordered  table-hover" id="sample_2">
									<thead>
										<tr>
											<th>序号</th>
											<th>申请人sid</th>
											<th>提现金额</th>
											<th>状态</th>
											<th>收款方式id</th>
											<th>申请提现时间</th>
											<th>审核时间</th>
											<th>审核人uid</th>
											<th>审核意见</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty marketingAccountList}">
											<c:forEach items="${marketingAccountList }" var="single" varStatus="status">
												<tr>
													<td>${status.count}</td>
													<td>${single.applySid }</td>
													<td>${single.money }</td>
													<td>${single.status }</td>
													<td>${single.paymentMethodId }</td>
													<td><fmt:formatDate value="${single.applyDate }" pattern="yyyy-MM-dd HH:mm"/></td>
													<td><fmt:formatDate value="${single.auditDate }" pattern="yyyy-MM-dd HH:mm"/></td>
													<td>${single.auditUid }</td>
													<td>${single.auditComments }</td>
													<td></td>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	
	<script type="text/javascript">
	
	$(document).ready(function() {
		
		$("#weixinPublicId").bind('change',function(){
			var wpid=$("#weixinPublicId").val();
			window.location="${ctx}/fin/withdrawalApply/"+wpid;
		});
	});
	</script>
	<script type="text/javascript">
	$(document).ready(function() {
		
		initTable2();
		
	});
	
	var initTable2 = function() {
	    var oTable = $('#sample_2').dataTable( {           
	        "aoColumnDefs": [
	            { "aTargets": [ 0 ] }
	        ],
	        "aaSorting": [[0, 'asc']],
	         "aLengthMenu": [
	            [5, 15, 20, -1],
	            [5, 15, 20, "All"] // change per page values here
	        ],
	        // set the initial value
	        "iDisplayLength": 10,
	    });
	
	    jQuery('#sample_2_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
	    jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
	    jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown
	
	    $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
	        /* Get the DataTables object again - this is not a recreation, just a get of the object */
	        var iCol = parseInt($(this).attr("data-column"));
	        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
	        oTable.fnSetColumnVis(iCol, (bVis ? false : true));
	    });
	}
	
	function delConfirm(del_url){
		bootbox.dialog({
			  message: "删除后不可恢复，是否删除数据？",
			  title: "删除确认",
			  buttons: {
			    success: {
			      label: "取消",
			      className: "btn"
			    },
			    danger: {
			      label: "删除",
			      className: "btn black",
			      callback: function() {
			    	  location.href = del_url;
			      }
			    }
			  }
		});
	}
</script>
</body>
	
</html>


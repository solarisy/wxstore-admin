<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>微信公众号管理</title>


</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<c:if test="${not empty message}">
					<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close"></button>${message}</div>
				</c:if>
				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">微信公众号管理</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="${ctx}/weixin">微信公众号管理</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>编辑</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-weixinPublic"></i>微信公众号管理</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form id="inputForm" action="${ctx}/weixin/update" method="post" class="form-horizontal form-row-seperated">
									<input type="hidden" name="id" value="${weixinPublic.id}"/>
									<fieldset>
										<legend><small>公众号</small></legend>
										<div class="control-group">
											<label class="control-label">公众号名称</label>
											<div class="controls">
												<input type="text" name="publicMarkName" value="${weixinPublic.publicMarkName}" class="m-wrap span12" />
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">店名:</label>
											<div class="controls">
												<input type="text" name="marketingName" value="${weixinPublic.marketingName}" class="m-wrap span12" />
											</div>
										</div>
										<div class="control-group">
											<label for="plainPassword" class="control-label">appId</label>
											<div class="controls">
												<input type="text" id="appId" name="appId" value="${weixinPublic.appId}" class="m-wrap span12" />
											</div>
										</div>
										<div class="control-group">
											<label for="confirmPassword" class="control-label">appSecret</label>
											<div class="controls">
												<input type="text" id="appSecret" name="appSecret" value="${weixinPublic.appSecret}" class="m-wrap span12"  />
											</div>
										</div>
										<div class="control-group">
											<label for="confirmPassword" class="control-label">serverUrl</label>
											<div class="controls">
												<input type="text" id="serverUrl" name="serverUrl" value="${weixinPublic.serverUrl}" class="m-wrap span12"  />
											</div>
										</div>
										<div class="control-group">
											<label for="confirmPassword" class="control-label">token</label>
											<div class="controls">
												<input type="text" id="token" name="token" value="${weixinPublic.token}" class="m-wrap span12"  />
											</div>
										</div>
										<div class="control-group">
											<label for="confirmPassword" class="control-label">encodingAESKey</label>
											<div class="controls">
												<input type="text" id="encodingAESKey" name="encodingAESKey" value="${weixinPublic.encodingAESKey}" class="m-wrap span12"  />
											</div>
										</div>
										<div class="form-actions">
											<input id="submit_btn" class="btn btn-primary" type="submit" value="提交"/>&nbsp;	
											<input id="cancel_btn" class="btn" type="button" value="返回" onclick="history.back()"/>
										</div>
									</fieldset>
								</form>
								<!-- END FORM-->

							</div>


						</div>

		

						<!-- END BORDERED TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
</body>
	
</html>


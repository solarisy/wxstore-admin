<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>微信公众号管理</title>


</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<c:if test="${not empty message}">
					<script>
						$.gritter.add({
							class_name: 'my-sticky-class',
							// (int | optional) the time you want it to be alive for before fading out (milliseconds)
							time: 2000,
							// (bool | optional) if you want it to fade out on its own or just sit there
							sticky: true,
							// (string | mandatory) the heading of the notification
							title: '提示消息',
							// (string | mandatory) the text inside the notification
							text: '${message}'
						});
					</script>
				</c:if>
				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">微信公众号管理</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>微信公众号管理</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i>微信公众号管理</div>
								<div class="tools">
									<div class="btn-group">
										<a class="btn" href="#" data-toggle="dropdown">
										显示/隐藏列
										<i class="icon-angle-down"></i>
										</a>
										<div id="sample_2_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
											<label><input type="checkbox" checked data-column="1">公众号名称</label>
											<label><input type="checkbox" checked data-column="2">店名</label>
											<label><input type="checkbox" checked data-column="3">appId</label>
											<label><input type="checkbox" checked data-column="4">appSecret</label>
											<label><input type="checkbox" checked data-column="5">serverUrl</label>
											<label><input type="checkbox" checked data-column="6">encodingAESKey</label>
										</div>
									</div>
									<a class="btn" href="${ctx}/weixin/create">新增微信公众号</a>
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
									<thead>
										<tr>
											<th>##</th>
											<th>公众号名称</th>
											<th>店名</th>
											<th>appId</th>
											<th>appSecret</th>
											<th>serverUrl</th>
											<th>token</th>
											<th>encodingAESKey</th>
											<th>创建时间</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${weixinPublicList}" var="weixinPublic" varStatus="status">
										<tr>
											<td>${status.count}</td>
											<td><a href="${ctx}/weixin/update/${weixinPublic.id}">${weixinPublic.publicName}</a></td>
											<td>${weixinPublic.marketingName}</td>
											<td>${weixinPublic.appId}</td>
											<td>${weixinPublic.appSecret}</td>
											<td>${weixinPublic.serverUrl}</td>
											<td>${weixinPublic.token}</td>
											<td>${weixinPublic.encodingaeskey}</td>
											<td><fmt:formatDate value="${weixinPublic.createDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
											<td>
												<a href="javascript:delConfirm('${ctx}/weixin/delete/${weixinPublic.id}');" class="btn mini black"><i class="icon-trash"></i> 删除</a>
											</td>
										</tr>
										</c:forEach>
										
									</tbody>

								</table>

							</div>


						</div>
<script>
	
	$(document).ready(function() {
		
		initTable2();
		
		$.extend($.gritter.options, {
	        position: 'top-right', // defaults to 'top-right' but can be 'bottom-left', 'bottom-right', 'top-left', 'top-right' (added in 1.7.1)
			fade_in_speed: 'fast', // how fast notifications fade in (string or int)
			fade_out_speed: 1000, // how fast the notices fade out
			time: 3000 // hang on the screen for...
		});
	});
	
	var initTable2 = function() {
        var oTable = $('#sample_2').dataTable( {           
            "aoColumnDefs": [
                { "aTargets": [ 0 ] }
            ],
            "aaSorting": [[0, 'asc']],
             "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
        });

        jQuery('#sample_2_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
        jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
        jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
    }
	
	function delConfirm(del_url){
		bootbox.dialog({
			  message: "删除后不可恢复，是否删除数据？",
			  title: "删除确认",
			  buttons: {
			    success: {
			      label: "取消",
			      className: "btn"
			    },
			    danger: {
			      label: "删除",
			      className: "btn black",
			      callback: function() {
			    	  location.href = del_url;
			      }
			    }
			  }
		});
	}
	
</script>

						<!-- END BORDERED TABLE PORTLET-->

					</div>

				</div>

				<!-- END PAGE CONTENT-->

			</div>

			<!-- END PAGE CONTAINER-->

		</div>

		<!-- END PAGE -->

	</div>
</body>
	
</html>


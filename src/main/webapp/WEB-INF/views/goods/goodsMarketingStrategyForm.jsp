<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>商品</title>
</head>
<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<div id="portlet-config" class="modal hide">

				<div class="modal-header">

					<button data-dismiss="modal" class="close" type="button"></button>

					<h3>portlet Settings</h3>

				</div>

				<div class="modal-body">

					<p>Here will be a configuration form</p>

				</div>

			</div>

			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">商品</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>商品<i class="icon-angle-right"></i></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->

					</div>

				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->          

				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN VALIDATION STATES-->

						<div class="portlet box red">

							<div class="portlet-title">

								<div class="caption"><i class="icon-reorder"></i>商品</div>

								<div class="tools">

									<a href="javascript:;" class="collapse"></a>

								</div>

							</div>

							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form id="inputForm" action="${ctx}/goods/updateMarketingStrategy" method="post" class="form-horizontal form-row-seperated">
								<input type="hidden" name="goodsId" value="${goods.id}"/>
								<fieldset>
									<legend><small>商品【${goods.goodsName}】</small></legend>
									
									<div class="control-group">
										<label for="employeeName" class="control-label">一级提成  <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="levelOneBrokerage" name="levelOneBrokerage" value="${goodsMarketingStrategy.levelOneBrokerage }" class="m-wrap span10"/>(元)
										</div>
									</div>
									<div class="control-group">
										<label for="employeePhone" class="control-label">二级提成 <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="levelTwoBrokerage" name="levelTwoBrokerage" value="${goodsMarketingStrategy.levelTwoBrokerage }" class="m-wrap span10"/>(元)
										</div>
									</div>
									<div class="control-group">
										<label for="jvcId" class="control-label">三级提成 <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="levelThreeBrokerage" name="levelThreeBrokerage" value="${goodsMarketingStrategy.levelThreeBrokerage }" class="m-wrap span10"/>(元)
										</div>
									</div>
									<div class="form-actions">
										<input id="submit_btn" class="btn btn-primary" type="submit" value="提交"/>&nbsp;
										<input id="cancel_btn" class="btn" type="button" value="返回" onclick="history.back()"/>
									</div>
								</fieldset>
								</form>
								<!-- END FORM-->
							</div>

						</div>

						<!-- END VALIDATION STATES-->

					</div>

				</div>

				<!-- END PAGE CONTENT-->

			</div>

			<!-- END PAGE CONTAINER-->

		</div>

		<!-- END PAGE -->

	</div>

</body>
</html>

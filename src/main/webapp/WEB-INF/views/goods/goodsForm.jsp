<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<title>商品</title>
</head>
<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<div id="portlet-config" class="modal hide">

				<div class="modal-header">

					<button data-dismiss="modal" class="close" type="button"></button>

					<h3>portlet Settings</h3>

				</div>

				<div class="modal-body">

					<p>Here will be a configuration form</p>

				</div>

			</div>

			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">商品</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>商品<i class="icon-angle-right"></i></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->

					</div>

				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->          

				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN VALIDATION STATES-->

						<div class="portlet box red">

							<div class="portlet-title">

								<div class="caption"><i class="icon-reorder"></i>商品</div>

								<div class="tools">

									<a href="javascript:;" class="collapse"></a>

								</div>

							</div>

							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<form id="inputForm" action="${ctx}/goods/update" method="post" class="form-horizontal form-row-seperated">
								<input type="hidden" name="id" value="${goods.id}"/>
								<fieldset>
									<legend><small>商品</small></legend>
									
									<div class="control-group">
										<label class="control-label">所属公众号</label>
										<div class="controls">
											<select name="weixinPublicId" data-placeholder="选择所属公众号" class="chosen-with-diselect span10"  >
												<c:forEach items="${weixinPublicList}" var="weixinPublic" >
													<c:choose>
														<c:when test="${goods.weixinPublicId == weixinPublic.id}">
															<option selected="selected" value="${weixinPublic.id}">${weixinPublic.publicName}</option>
														</c:when>
														<c:otherwise>
															<option value="${weixinPublic.id}">${weixinPublic.publicName}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</select>
										</div>
									</div>
									
									<div class="control-group">
										<label for="employeeName" class="control-label">商品名  <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="goodsName" name="goodsName" value="${goods.goodsName }" class="m-wrap span10"/>
										</div>
									</div>
									<div class="control-group">
										<label for="employeePhone" class="control-label">商品描述  <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="goodsDesc" name="goodsDesc" value="${goods.goodsDesc }" class="m-wrap span10"/>
										</div>
									</div>
									<div class="control-group">
										<label for="jvcId" class="control-label">销售价格 <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="price" name="price" value="${goods.price }" class="m-wrap span10"/>(元)
										</div>
									</div>
									<div class="control-group">
										<label for="jvcId" class="control-label">真实价格  <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="realPrice" name="realPrice" value="${goods.realPrice }" class="m-wrap span10"/>(元)
										</div>
									</div>
									<div class="control-group">
										<label for="jvcId" class="control-label">商品详情URL  <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" id="goodsDetailsUrl" name="goodsDetailsUrl" value="${goods.goodsDetailsUrl }" class="m-wrap span10"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">上否上架  <font style="color: red">*</font></label>
										<div class="controls">
											<label class="radio">
											<input type="radio" id="goodsStatus" name="goodsStatus" value="1" <c:if test="${goods.goodsStatus == 1 }">checked="checked"</c:if>/>
											是
											</label>
											<label class="radio">
											<input type="radio" id="goodsStatus" name="goodsStatus" value="2" <c:if test="${goods.goodsStatus == 2 }">checked="checked"</c:if>/>
											否
											</label>  
										</div>
									</div>
									<div id="message">
									</div>
									<br/><br/><br/>
									<div class="form-actions">
										<input id="submit_btn" class="btn btn-primary" type="submit" value="提交"/>&nbsp;
										<input id="cancel_btn" class="btn" type="button" value="返回" onclick="history.back()"/>
									</div>
								</fieldset>
								</form>
								<!-- END FORM-->
							</div>

						</div>

						<!-- END VALIDATION STATES-->

					</div>

				</div>

				<!-- END PAGE CONTENT-->

			</div>

			<!-- END PAGE CONTAINER-->

		</div>

		<!-- END PAGE -->

	</div>

</body>
</html>

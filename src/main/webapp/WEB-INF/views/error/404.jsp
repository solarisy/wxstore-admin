<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%response.setStatus(200);%>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>好享家</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="format-detection" content="telephone=no" />
<link href="${ctx}/static/css/font-awesome.min.css" rel="stylesheet">
<link href="${ctx}/static/css/common.css?v=1.0" rel="stylesheet">
</head>
<body style="padding-bottom:6rem">
<header class="header">
    <span class="colfff font16 span"></span>
    <a href="javascript:history.back();" class="back">
      <i class="fa fa-angle-left"></i>
    </a>
</header>
<p class="center">
<img src="${ctx}/static/images/404.png" style="margin-top:10rem; width:100%;" />
</p>




<%@ include file="/WEB-INF/layouts/footer.jsp"%>


<script type="text/javascript" src="${ctx}/static/js/jquery.js"></script>
<script type="text/javascript" src="${ctx}/static/js/idangerous.swiper-1.9.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/js/pub.js"></script>
</body>
<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory" %>
<%	
	//设置返回码200，避免浏览器自带的错误页面
	response.setStatus(200);
	//记录日志
	Logger logger = LoggerFactory.getLogger("500.jsp");
	logger.error(exception.getMessage(), exception);
	
	
%>


<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>好享家</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta name="format-detection" content="telephone=no" />
<link href="${ctx}/static/css/font-awesome.min.css" rel="stylesheet">
<link href="${ctx}/static/css/common.css?v=1.0" rel="stylesheet">
</head>
<body style="padding-bottom:6rem">
<header class="header">
    <span class="colfff font16 span"></span>
    <a href="javascript:history.back();" class="back">
      <i class="fa fa-angle-left"></i>
    </a>
</header>
<p class="center">
<img src="${ctx}/static/images/503.png" style="margin-top:10rem; width:100%;" />
</p>

<p class="center top20 pad10"><a href="" class="btns btns-biggest btns-ye">手机号重复</a></p>

<p class="center top20 pad10"><a href="${ctx}/my/unbindingWeixin" class="btns btns-biggest btns-ye">退出</a></p>



<%@ include file="/WEB-INF/layouts/footer.jsp"%>


<script type="text/javascript" src="${ctx}/static/js/jquery.js"></script>
<script type="text/javascript" src="${ctx}/static/js/idangerous.swiper-1.9.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/js/pub.js"></script>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>账号管理</title>


</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<c:if test="${not empty message}">
					<script>
						$.gritter.add({
							class_name: 'my-sticky-class',
							// (int | optional) the time you want it to be alive for before fading out (milliseconds)
							time: 2000,
							// (bool | optional) if you want it to fade out on its own or just sit there
							sticky: true,
							// (string | mandatory) the heading of the notification
							title: '提示消息',
							// (string | mandatory) the text inside the notification
							text: '${message}'
						});
					</script>
				</c:if>
				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">账号管理</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>账号管理</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>账号管理</div>
								<div class="tools">
									<a class="btn" href="${ctx}/register">注册账号</a>
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							
							<div class="portlet-body">
								<table class="table table-striped table-bordered  table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>登录名</th>
											<th>用户名</th>
											<th>注册时间</th>
											<th>管理</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${users}" var="user" varStatus="status">
										<tr>
											<td>${status.count}</td>
											<td><a href="${ctx}/admin/user/update/${user.id}">${user.loginName}</a></td>
											<td>${user.name}</td>
											<td><fmt:formatDate value="${user.createDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
											<td>
												<c:choose>
													<c:when test="${user.id == 1}"></c:when>
													<c:otherwise>
														<a href="javascript:delConfirm('${ctx}/admin/user/delete/${user.id}');" class="btn mini black"><i class="icon-trash"></i> 删除</a>
													</c:otherwise>
												</c:choose>
												
											</td>
										</tr>
										</c:forEach>
										
									</tbody>

								</table>

							</div>


						</div>
	<script>
	
	$(document).ready(function() {
		$.extend($.gritter.options, {
	        position: 'top-right', // defaults to 'top-right' but can be 'bottom-left', 'bottom-right', 'top-left', 'top-right' (added in 1.7.1)
			fade_in_speed: 'fast', // how fast notifications fade in (string or int)
			fade_out_speed: 1000, // how fast the notices fade out
			time: 3000 // hang on the screen for...
		});
	});
	
		function delConfirm(del_url){
			bootbox.dialog({
				  message: "删除后不可恢复，是否删除数据？",
				  title: "删除确认",
				  buttons: {
				    success: {
				      label: "取消",
				      className: "btn"
				    },
				    danger: {
				      label: "删除",
				      className: "btn black",
				      callback: function() {
				    	  location.href = del_url;
				      }
				    }
				  }
			});
		}
	
    </script>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/>
		

						<!-- END BORDERED TABLE PORTLET-->

					</div>

				</div>

				<!-- END PAGE CONTENT-->

			</div>

			<!-- END PAGE CONTAINER-->

		</div>

		<!-- END PAGE -->

	</div>
</body>
	
</html>


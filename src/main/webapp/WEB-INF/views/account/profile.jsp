<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>修改密码</title>


</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<div id="portlet-config" class="modal hide">

				<div class="modal-header">

					<button data-dismiss="modal" class="close" type="button"></button>

					<h3>portlet Settings</h3>

				</div>

				<div class="modal-body">

					<p>Here will be a configuration form</p>

				</div>

			</div>

			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">修改密码</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>修改密码<i class="icon-angle-right"></i></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->

					</div>

				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->          

				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN VALIDATION STATES-->

						<div class="portlet box red">

							<div class="portlet-title">

								<div class="caption"><i class="icon-reorder"></i>修改密码</div>

								<div class="tools">

									<a href="javascript:;" class="collapse"></a>

								</div>

							</div>

							<div class="portlet-body form">

								<!-- BEGIN FORM-->

								<form id="inputForm" action="${ctx}/profile/update" class="form-horizontal" method="post">
									<input type="hidden" name="id" value="${user.id}"/>
									<div class="control-group">

										<label class="control-label">登录账号</label>

										<div class="controls">

											<input type="text" readonly="readonly" name="loginName" value="${user.loginName}" id="loginName" data-required="1" class="span6 m-wrap required"/>

										</div>

									</div>
									
									<div class="control-group">

										<label class="control-label">真实姓名<span class="required">*</span></label>

										<div class="controls">

											<input type="text" id="name" name="name" readonly="readonly" value="${user.name}" data-required="1" class="span6 m-wrap"/>

										</div>

									</div>
									
									<div class="control-group">

										<label class="control-label">密&nbsp;&nbsp;码<span class="required">*</span></label>

										<div class="controls">

											<input name="plainPassword" id="plainPassword" type="password" class="span6 m-wrap"/>

										</div>

									</div>

									<div class="control-group">

										<label class="control-label">确认密码<span class="required">*</span></label>

										<div class="controls">

											<input name="repassword" id="repassword" type="password" class="span6 m-wrap"/>

										</div>

									</div>

									
									<input type="hidden" id = "id" name = "id" value = "${user.id }"/>
									<input type="hidden" id = "roles" name = "roles" value = "${user.roles }"/>
									<input type="hidden" id = "uid" name = "uid" value = "${user.uid }"/>
								</form>

								<!-- END FORM-->
									<div class="form-actions">

										<button onclick="doSubmit();" class="btn purple">保存</button>

										<button type="button" class="btn" onclick="history.back()">返回</button>

									</div>

							</div>

						</div>

						<!-- END VALIDATION STATES-->

					</div>

				</div>

				<!-- END PAGE CONTENT-->

			</div>

			<!-- END PAGE CONTAINER-->

		</div>

		<!-- END PAGE -->

	</div>
	<script type="text/javascript" src="${ctx }/static/js/jquery.js"></script>
	<script type="text/javascript">
		String.prototype.trim = function () {  
		    return this.replace(/(^\s*)|(\s*$)/g, "");  
		}  
		
		function doSubmit(){
			var password = $("#plainPassword").val().trim();
			var repassword = $("#repassword").val().trim();
			if(password.length < 5 ){
				alert("密码最少五位！");
				return;
				$("#inputForm").submit(false);
			}else if(password != repassword){
				alert("两次密码不一样！");
				return;
				$("#inputForm").submit(false);
			}
			$("#inputForm").submit();
		}
	</script>
</body>
	
</html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>订单管理</title>

</head>

<script type="text/javascript" src="${ctx }/static/js/jquery.js"></script>
<script type="text/javascript">

</script>
   
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->
			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>
		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->        
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">订单管理</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="${ctx}/order">订单管理</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>订单详情</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-euro"></i>订单详情 -- 订单编号【${goodsOrder.orderCode}】<input id="cancel_btn" class="btn" type="button" value="返回" onclick="history.back()"/></div>
								<div class="tools">
									
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							
							<div class="portlet-body">
								<table class="table sliders table-striped">
									<tbody>
										<tr>
											<td style="width:50%">订单编号:<span class="badge badge-inverse">${goodsOrder.orderCode}</span></td>
											<td>
												订单状态:<span class="badge badge-inverse">${goodsOrder.orderStatusName}</span>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="table sliders table-striped table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th style="width:15%">商品名称</th>
											<th>商品描述</th>
											<th>单价</th>
											<th>商品数量</th>
											<th>小计</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${detailList }" var="detail" varStatus="status">
										<tr>
											<td>${status.count}</td>
											<td>${detail.goodsName}</td>
											<td>${detail.goodsDesc}</td>
											<td>${detail.realPrice}</td>
											<td>${detail.goodsNumber}</td>
											<td>${detail.total}</td>
										</tr>
										</c:forEach>
										
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>共<span class="badge badge-info">${allGoodsNumber}</span>件</td>
											<td>实付:<span class="label label-warning">${allRealPay} (元)</span></td>
										</tr>
									</tbody>
								</table>

							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>

				</div>

				<!-- END PAGE CONTENT-->

			</div>

			<!-- END PAGE CONTAINER-->

		</div>

		<!-- END PAGE -->

	</div>
</body>
	
</html>


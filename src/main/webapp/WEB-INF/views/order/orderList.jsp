<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

	<meta charset="utf-8" />

	<title>订单管理</title>

</head>

<script type="text/javascript" src="${ctx }/static/js/jquery.js"></script>
<script type="text/javascript">

</script>
   
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

	<!-- BEGIN CONTAINER -->

	<div class="page-container row-fluid">

		<!-- BEGIN SIDEBAR -->

			<!-- Import Menu -->        
			<jsp:include page="/WEB-INF/layouts/menu.jsp"></jsp:include>

		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->

		<div class="page-content">

			<!-- BEGIN PAGE CONTAINER-->        

			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->

				<%-- <c:if test="${not empty message}">
					<div id="message" class="alert alert-success"><button data-dismiss="alert" class="close"></button>${message}</div>
				</c:if> --%>
				<div class="row-fluid">

					<div class="span12">

						<!-- BEGIN STYLE CUSTOMIZER -->
						<!-- Import Style Setting Button -->        
						<jsp:include page="/WEB-INF/layouts/style-setting.jsp"></jsp:include>
						<!-- END BEGIN STYLE CUSTOMIZER -->  

						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">订单管理</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="${ctx}">首页</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>订单管理</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>

				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-euro"></i>订单管理</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							
							<div class="portlet-body">
								<table class="table table-striped table-bordered  table-hover">
									<thead>
										<tr>
											<th>序号</th>
											<th>订单编号</th>
											<th>微信公众号id</th>
											<th>订单价格</th>
											<th>实付价格</th>
											<th>订单状态</th>
											<th>订货人</th>
											<th>是否取消订单</th>
											<th>创建时间</th>
											<th>最后更新时间</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty list}">
											<c:forEach items="${list }" var="single" varStatus="status">
												<tr>
													<td>${status.count}</td>
													<td><a href="${ctx}/order/detail/${single.id}">${single.orderCode}</a></td>
													<td>${single.weixinPublicId }</td>
													<td>${single.money }</td>
													<td>${single.realMoney }</td>
													<td>${single.orderStatusName }</td>
													<td>${single.nickname }</td>
													<td>${single.canceled }</td>
													<td><fmt:formatDate value="${single.createDate }" pattern="yyyy-MM-dd HH:mm"/></td>
													<td><fmt:formatDate value="${single.updateDate }" pattern="yyyy-MM-dd HH:mm"/></td>
													<td>
													</td>
												</tr>
											</c:forEach>
										</c:if>
										<c:if test="${empty list}">
											<tr><td colspan="13" style="text-align: center;"><font style="color:red;text-align: center;">暂无订单！</font></td></tr>
										</c:if>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
</body>
	
</html>


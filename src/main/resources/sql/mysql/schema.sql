/*
Navicat MySQL Data Transfer

Source Server         : 192.168.6.149
Source Server Version : 50621
Source Host           : 192.168.6.149:3306
Source Database       : ocw

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-06-09 09:25:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wx_weixin_public
-- ----------------------------
DROP TABLE IF EXISTS `wx_weixin_public`;
CREATE TABLE `wx_weixin_public` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime DEFAULT NULL COMMENT '最后更新时间',
  `create_user_uid` varchar(128) DEFAULT NULL COMMENT '创建人uid',
  `update_user_uid` varchar(128) DEFAULT NULL COMMENT '最后更新人uid',
  `deleted` int(11) DEFAULT NULL COMMENT '是否删除（0：删，1：用）',
  `app_id` varchar(128) DEFAULT NULL,
  `app_secret` varchar(128) DEFAULT NULL,
  `server_url` varchar(1024) DEFAULT NULL,
  `token` varchar(128) DEFAULT NULL,
  `encodingAESKey` varchar(128) DEFAULT NULL,
  `redirect_uil` varchar(1024) DEFAULT NULL,
  `public_mark_name` varchar(128) DEFAULT NULL,
  `public_mark_password` varchar(128) DEFAULT NULL,
  `marketing_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='微信公众号信息（营销号）';

-- ----------------------------
-- Records of wx_weixin_public
-- ----------------------------
INSERT INTO `wx_weixin_public` VALUES ('1', '2015-06-05 14:06:45', '2015-06-05 14:06:48', '1', '1', '1', 'wx42cc39e11c2ef1a3', 'fc25945d3a17441c8bc46c6381002ea5', 'http://218.2.192.149/ocw/wserver', 'hosjoy_test_token', 'vedHUzMxEyPp0wHw3Tx685iqcCxTMcEofOSO0CeLYIh', null, '好享家舒适智能家居', '123456', '六安大吉茶庄');
